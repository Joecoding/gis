import * as t from "./types";

export const setLang = (locale) => (dispatch) => {
  dispatch({
    type: t.SET_LANG,
    payload: locale,
  });
};
