import * as t from "./../actions/types";

const initialState = {};

export default function language(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case t.SET_LANG:
      return {
        ...state,
        locale: payload,
      };
    default:
      return state;
  }
}
