import { TextField } from "@mui/material";
import Layout from "../components/shared/Layout";
import Link from "next/link";

import styles from "./../styles/Login.module.scss";

const Login = ({}) => {
  return (
    <>
      <Layout>
        <main className="py-8">
          <div className="container">
            <div className="row">
              <div className="d-none d-md-block col-md-3 col-lg-4"></div>
              <div className="col-md-6 col-lg-4">
                <h5 className="mt-5 mb-3">LOG IN</h5>
                <div className="d-flex flex-column align-items-center gap-3">
                  <button
                    className={`${styles["login-btn"]} ${styles["facebook"]}`}
                  >
                    <span>
                      <img
                        src="/img/icons/facebook.png"
                        alt="facebook"
                        className="w-100"
                      />
                    </span>
                    Continue with Facebook
                  </button>
                  <button
                    className={`${styles["login-btn"]} ${styles["google"]}`}
                  >
                    <span>
                      <img
                        src="/img/icons/google.png"
                        alt="facebook"
                        className="w-100"
                      />
                    </span>
                    Continue with Google
                  </button>
                  <div className={styles["strike"]}>
                    <span>or</span>
                  </div>

                  <TextField
                    label="Your email id"
                    variant="outlined"
                    size="large"
                    sx={{ width: "100%" }}
                  />
                  <TextField
                    label="Your password"
                    variant="outlined"
                    size="large"
                    sx={{ width: "100%" }}
                  />
                  <button
                    className={`${styles["login-btn"]}`}
                    aria-disabled="true"
                  >
                    Login
                  </button>
                  <div className={`${styles["login-footer"]} text-gray`}>
                    <p className="text-center mb-2">
                      <Link href="/#">Forgot your passwork?</Link>
                    </p>
                    <p className="text-center mb-2">
                      New to giveats? <Link href="/#">Create account</Link>
                    </p>
                    <small className="text-center">
                      By continuing, you agree to our T&Cs and Privacy Policy,
                      for more details click <Link href="/#">here</Link>
                    </small>
                  </div>
                </div>
              </div>
              <div className="d-none d-md-block col-md-3 col-lg-4"></div>
            </div>
          </div>
        </main>
      </Layout>
    </>
  );
};

export default Login;
