import React, { useState, useEffect, Fragment, useRef } from "react";
import Link from "next/link";

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";

import styles from "./../../../styles/RestaurantPage/RestaurantMenu.module.scss";

import FoodItemDialog from "./../../../components/RestaurantPage/FoodItemDialog";

const foodData = [
  {
    section: "Formula",
    name: "food 1",
  },
  {
    section: "Formula",
    name: "food 2",
  },
  {
    section: "1 bought = 1 free",
    name: "food 3",
  },
  {
    section: "1 bought = 1 free",
    name: "food 3",
  },
  {
    section: "1 bought = 1 free",
    name: "food 3",
  },
  {
    section: "Entrance",
    name: "food 3",
  },
  {
    section: "Entrance",
    name: "food 3",
  },
  {
    section: "Flat",
    name: "food 3",
  },
  {
    section: "Flat",
    name: "food 3",
  },
  {
    section: "Desserts",
    name: "food 3",
  },
  {
    section: "Desserts",
    name: "food 3",
  },
];

import RestaurantPageParent from "../../../components/RestaurantPage/RestaurantPageParent";

const RestaurantMenuPage = () => {
  const [tabs] = useState([]);
  const [selectedTab, setSelectedTab] = useState("");

  const foodItemsHorizontal = useRef();

  useEffect(() => {
    foodData.forEach((el) => {
      if (!tabs.includes(el.section)) {
        tabs.push(el.section);
      }
    });
    setSelectedTab(tabs[0]);

    if (foodItemsHorizontal.current) {
      foodItemsHorizontal.current.addEventListener("wheel", (evt) => {
        evt.preventDefault();
        foodItemsHorizontal.current.scrollLeft += evt.deltaY;
    });
    }
  }, [foodItemsHorizontal]);

  const makeSlug = (str) => {
    return str.toLowerCase().split(" ").join("-");
  };

  return (
    <>
      <RestaurantPageParent>
        <main id={styles["menu-section"]} className="container">
          <div className="row">
            <div className="d-none d-md-block col-md-2"></div>
            <div className="col-md-8">
              <div
                id={styles["browse-menu"]}
                className="d-flex align-items-center justify-content-center gap-2 py-3 mx-2"
              >
                {tabs.length !== 0 &&
                  tabs.map((tab) => {
                    return (
                      <>
                        <Link href={`#${makeSlug(tab)}`}>
                          <div
                            onClick={() => setSelectedTab(tab)}
                            className={`${styles["tab"]} ${
                              selectedTab === tab ? styles["active"] : ""
                            }`}
                          >
                            {tab}
                          </div>
                        </Link>
                      </>
                    );
                  })}
              </div>
              <div className={`${styles["retaurant-suggestion"]} my-4`}>
                <h4 className="fw-bolder mb-4">Chef's suggestion</h4>
                <div ref={foodItemsHorizontal} className={`${styles["items-scroll"]}`}>
                  <div className={styles["food-item"]}>
                    <div className={styles["food-item-card"]}>
                      <div
                        className={`${styles["food-img"]} d-flex flex-column align-items-center`}
                      >
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="Food Image"
                          className="w-100"
                        />
                      </div>
                      <div className={styles["food-price"]}>$ 12.50</div>
                    </div>
                    <div className="mb-1 mt-4">
                      <h5>Sweet Bagel</h5>
                      <div
                        className={`${styles["food-item-action"]} d-flex align-items-center gap-2`}
                        style={{ marginTop: "-6px" }}
                      >
                        <AddCircleOutlineIcon fontSize="small" />
                        <p className="m-0">2</p>
                        <RemoveCircleOutlineIcon fontSize="small" />
                      </div>
                    </div>
                  </div>
                  <div className={styles["food-item"]}>
                    <div className={styles["food-item-card"]}>
                      <div
                        className={`${styles["food-img"]} d-flex flex-column align-items-center`}
                      >
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="Food Image"
                          className="w-100"
                        />
                      </div>
                      <div className={styles["food-price"]}>$ 12.50</div>
                    </div>
                    <div className="mb-1 mt-4">
                      <h5>Sweet Bagel</h5>
                      <div
                        className={`${styles["food-item-action"]} d-flex align-items-center gap-2`}
                        style={{ marginTop: "-6px" }}
                      >
                        <AddCircleOutlineIcon fontSize="small" />
                        <p className="m-0">2</p>
                        <RemoveCircleOutlineIcon fontSize="small" />
                      </div>
                    </div>
                  </div><div className={styles["food-item"]}>
                    <div className={styles["food-item-card"]}>
                      <div
                        className={`${styles["food-img"]} d-flex flex-column align-items-center`}
                      >
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="Food Image"
                          className="w-100"
                        />
                      </div>
                      <div className={styles["food-price"]}>$ 12.50</div>
                    </div>
                    <div className="mb-1 mt-4">
                      <h5>Sweet Bagel</h5>
                      <div
                        className={`${styles["food-item-action"]} d-flex align-items-center gap-2`}
                        style={{ marginTop: "-6px" }}
                      >
                        <AddCircleOutlineIcon fontSize="small" />
                        <p className="m-0">2</p>
                        <RemoveCircleOutlineIcon fontSize="small" />
                      </div>
                    </div>
                  </div><div className={styles["food-item"]}>
                    <div className={styles["food-item-card"]}>
                      <div
                        className={`${styles["food-img"]} d-flex flex-column align-items-center`}
                      >
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="Food Image"
                          className="w-100"
                        />
                      </div>
                      <div className={styles["food-price"]}>$ 12.50</div>
                    </div>
                    <div className="mb-1 mt-4">
                      <h5>Sweet Bagel</h5>
                      <div
                        className={`${styles["food-item-action"]} d-flex align-items-center gap-2`}
                        style={{ marginTop: "-6px" }}
                      >
                        <AddCircleOutlineIcon fontSize="small" />
                        <p className="m-0">2</p>
                        <RemoveCircleOutlineIcon fontSize="small" />
                      </div>
                    </div>
                  </div>
                  <div className={styles["food-item"]}>
                    <div className={styles["food-item-card"]}>
                      <div
                        className={`${styles["food-img"]} d-flex flex-column align-items-center`}
                      >
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="Food Image"
                          className="w-100"
                        />
                      </div>
                      <div className={styles["food-price"]}>$ 12.50</div>
                    </div>
                    <div className="mb-1 mt-4">
                      <h5>Sweet Bagel</h5>
                      <div
                        className={`${styles["food-item-action"]} d-flex align-items-center gap-2`}
                        style={{ marginTop: "-6px" }}
                      >
                        <AddCircleOutlineIcon fontSize="small" />
                        <p className="m-0">2</p>
                        <RemoveCircleOutlineIcon fontSize="small" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id={styles["category-list"]}>
                {foodData.map((food, i) => {
                  return (
                    <Fragment key={i}>
                      {i === 0 ? (
                        <>
                          <h4
                            id={makeSlug(food.section)}
                            className="fw-bolder my-3"
                          >
                            {food.section}
                          </h4>
                        </>
                      ) : i !== 0 &&
                        foodData[i].section !== foodData[i - 1].section ? (
                        <>
                          <h4
                            id={makeSlug(food.section)}
                            className="fw-bolder my-3"
                          >
                            {food.section}
                          </h4>
                        </>
                      ) : (
                        ""
                      )}
                      <FoodItemDialog title={food.name}>
                        <div
                          className={`${
                            styles["food-item"]
                          } my-2 pb-4 d-flex align-items-center ${
                            selectedTab === food.section ? styles["active"] : ""
                          }`}
                        >
                          <div className={styles["food-item-card"]}>
                            <div
                              className={`${styles["food-img"]} d-flex flex-column align-items-center`}
                            >
                              <img
                                src="/img/bagel-cover.jpg"
                                alt="Food Image"
                                className="w-100"
                              />
                            </div>
                            <div className={styles["food-price"]}>$ 12.50</div>
                          </div>
                          <div className="my-3 mx-4">
                            <h5>{food.name}</h5>
                            <div
                              className={`${styles["food-item-action"]} d-flex align-items-center gap-2`}
                              style={{ marginTop: "-6px" }}
                            >
                              <AddCircleOutlineIcon fontSize="small" />
                              <p className="m-0">2</p>
                              <RemoveCircleOutlineIcon fontSize="small" />
                            </div>
                          </div>
                        </div>
                      </FoodItemDialog>
                    </Fragment>
                  );
                })}
              </div>
            </div>
            <div className="d-none d-md-block col-md-2"></div>
          </div>
        </main>
        {/* <div>working</div> */}
      </RestaurantPageParent>
    </>
  );
};

export default RestaurantMenuPage;

export async function getServerSideProps(context) {
  console.log(context);
  return {
    props: {}, // will be passed to the page component as props
  };
}
