import React from "react";

import Rating from "@mui/material/Rating";
import Divider from "@mui/material/Divider";
import Star from "@mui/icons-material/Star";

import RestaurantPageParent from "../../../components/RestaurantPage/RestaurantPageParent";

import styles from "../../../styles/RestaurantPage/RestaurantOpinion.module.scss";

const RestaurantOpinionPage = () => {
  return (
    <>
      <RestaurantPageParent>
        <main id={styles["opinion-section"]} className="container">
          <div className="row">
            <div className="d-none d-md-block col-md-2"></div>
            <div className="col-md-8 my-5">
              <article
                className={`${styles["rating"]} d-flex align-items-center`}
              >
                <div className={`${styles["avg-rating"]} px-4 py-2`}>
                  <h2 className="m-0">3.4</h2>
                  <div>
                    <Rating
                      // name="half-rating-read"
                      value={4.5}
                      precision={0.5}
                      readOnly
                      icon={<Star sx={{ color: "#700cbe" }} />}
                    />
                  </div>
                  <small className="text-muted">52 notes</small>
                </div>
                <Divider
                  orientation="vertical"
                  flexItem
                  sx={{ borderRight: "3px solid lightgray" }}
                />
                <div
                  className={`${styles["indi-rating"]} flex-grow-1 px-4 py-2`}
                >
                  <div className="d-flex align-items-center gap-2">
                    <div className="d-flex align-items-center">
                      <span className="fw-bolder mt-1">5</span>
                      <Star
                        fontSize="small"
                        sx={{ color: "#700cbe", padding: "0px" }}
                      />
                    </div>
                    <div className="progress-bar">
                      <div
                        className="progress-meter"
                        style={{
                          width: "60%",
                        }}
                      ></div>
                    </div>
                    <small className="text-muted">10</small>
                  </div>
                  <div className="d-flex align-items-center gap-2">
                    <div className="d-flex align-items-center">
                      <span className="fw-bolder mt-1">4</span>
                      <Star
                        fontSize="small"
                        sx={{ color: "#700cbe", padding: "0px" }}
                      />
                    </div>
                    <div className="progress-bar">
                      <div
                        className="progress-meter"
                        style={{
                          width: "60%",
                        }}
                      ></div>
                    </div>
                    <small className="text-muted">10</small>
                  </div>
                  <div className="d-flex align-items-center gap-2">
                    <div className="d-flex align-items-center">
                      <span className="fw-bolder mt-1">3</span>
                      <Star
                        fontSize="small"
                        sx={{ color: "#700cbe", padding: "0px" }}
                      />
                    </div>
                    <div className="progress-bar">
                      <div
                        className="progress-meter"
                        style={{
                          width: "60%",
                        }}
                      ></div>
                    </div>
                    <small className="text-muted">10</small>
                  </div>
                  <div className="d-flex align-items-center gap-2">
                    <div className="d-flex align-items-center">
                      <span className="fw-bolder mt-1">2</span>
                      <Star
                        fontSize="small"
                        sx={{ color: "#700cbe", padding: "0px" }}
                      />
                    </div>
                    <div className="progress-bar">
                      <div
                        className="progress-meter"
                        style={{
                          width: "60%",
                        }}
                      ></div>
                    </div>
                    <small className="text-muted">10</small>
                  </div>
                  <div className="d-flex align-items-center gap-2">
                    <div className="d-flex align-items-center">
                      <span className="fw-bolder mt-1">1</span>
                      <Star
                        fontSize="small"
                        sx={{ color: "#700cbe", padding: "0px" }}
                      />
                    </div>
                    <div className="progress-bar">
                      <div
                        className="progress-meter"
                        style={{
                          width: "60%",
                        }}
                      ></div>
                    </div>
                    <small className="text-muted">10</small>
                  </div>
                </div>
              </article>
              <article>
                <div className="row my-5">
                  <div className="col-md-6 py-3">
                    <div
                      className={`${styles["review-card"]} p-3`}
                      style={{
                        boxShadow: "3px 3px 9px -4px black",
                        borderRadius: "20px",
                      }}
                    >
                      <div className="d-flex align-itmes-center gap-3">
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="profile"
                          className="rounded-circle"
                          height={50}
                          width={50}
                        />
                        <div className="flex-grow-1 ">
                          <h5 className="m-0">Customer name</h5>
                          <small className="text-muted">14 feb 2020</small>
                        </div>
                      </div>
                      <Rating
                        // name="half-rating-read"
                        value={4.0}
                        precision={0.5}
                        readOnly
                        icon={<Star sx={{ color: "#700cbe" }} />}
                        sx={{ margin: ".5rem 0rem" }}
                      />
                      <p className="text-muted">
                        Lorem ipsum ne wtex asdfon dust sisn
                      </p>
                      <div className="d-flex align-items-center flex-wrap gap-2">
                        <button className="btn-secondary px-2 py-1">
                          <small>Nice Team</small>
                        </button>
                        <button className="btn-secondary px-2 py-1">
                          <small>Neighborhood Nugget</small>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 py-3">
                    <div
                      className={`${styles["review-card"]} p-3`}
                      style={{
                        boxShadow: "3px 3px 9px -4px black",
                        borderRadius: "20px",
                      }}
                    >
                      <div className="d-flex align-itmes-center gap-3">
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="profile"
                          className="rounded-circle"
                          height={50}
                          width={50}
                        />
                        <div className="flex-grow-1 ">
                          <h5 className="m-0">Customer name</h5>
                          <small className="text-muted">14 feb 2020</small>
                        </div>
                      </div>
                      <Rating
                        // name="half-rating-read"
                        value={4.0}
                        precision={0.5}
                        readOnly
                        icon={<Star sx={{ color: "#700cbe" }} />}
                        sx={{ margin: ".5rem 0rem" }}
                      />
                      <p className="text-muted">
                        Lorem ipsum ne wtex asdfon dust sisn
                      </p>
                      <div className="d-flex align-items-center flex-wrap gap-2">
                        <button className="btn-secondary px-2 py-1">
                          <small>Nice Team</small>
                        </button>
                        <button className="btn-secondary px-2 py-1">
                          <small>Neighborhood Nugget</small>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 py-3">
                    <div
                      className={`${styles["review-card"]} p-3`}
                      style={{
                        boxShadow: "3px 3px 9px -4px black",
                        borderRadius: "20px",
                      }}
                    >
                      <div className="d-flex align-itmes-center gap-3">
                        <img
                          src="/img/bagel-cover.jpg"
                          alt="profile"
                          className="rounded-circle"
                          height={50}
                          width={50}
                        />
                        <div className="flex-grow-1 ">
                          <h5 className="m-0">Customer name</h5>
                          <small className="text-muted">14 feb 2020</small>
                        </div>
                      </div>
                      <Rating
                        // name="half-rating-read"
                        value={4.0}
                        precision={0.5}
                        readOnly
                        icon={<Star sx={{ color: "#700cbe" }} />}
                        sx={{ margin: ".5rem 0rem" }}
                      />
                      <p className="text-muted">
                        Lorem ipsum ne wtex asdfon dust sisn
                      </p>
                      <div className="d-flex align-items-center flex-wrap gap-2">
                        <button className="btn-secondary px-2 py-1">
                          <small>Nice Team</small>
                        </button>
                        <button className="btn-secondary px-2 py-1">
                          <small>Neighborhood Nugget</small>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </article>
            </div>
            <div className="d-none d-md-block col-md-2"></div>
          </div>
        </main>
      </RestaurantPageParent>
    </>
  );
};

export default RestaurantOpinionPage;

export async function getServerSideProps(context) {
  console.log(context);
  return {
    props: {}, // will be passed to the page component as props
  };
}
