import React from "react";

import RestaurantPageParent from "../../../components/RestaurantPage/RestaurantPageParent";

import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";

import styles from "../../../styles/RestaurantPage/RestaurantInfo.module.scss";

const RestaurantInfoPage = () => {
  return (
    <>
      <RestaurantPageParent>
        <section id={styles["info-section"]} className="container">
          <div className="row">
            <div className="d-none d-md-block col-md-2"></div>
            <div className="col-md-8 my-5">
              <div className="row">
                <div className="col-md-6">
                  <section className="p-3">
                    <h4 className="m-0">Address</h4>
                    <p className="text-muted">123 Street, Sample address</p>
                    <button className="btn-primary">
                      Get there&nbsp;
                      <ArrowRightAltIcon />
                    </button>
                  </section>
                  <section className="p-3">
                    <h4 className="m-0">Schedule</h4>
                    <p className="text-muted">123 Street, Sample address</p>
                    <button className="btn-secondary">Currently Open</button>
                  </section>
                  <section className="p-3">
                    <h4 className="m-0">Means of payment</h4>
                    <div
                      className="d-flex align-items-center flex-wrap gap-2 py-2"
                      style={{}}
                    >
                      <span
                        className="py-2 px-3 rounded-pill"
                        style={{ backgroundColor: "lightgray" }}
                      >
                        Cash
                      </span>
                      <span
                        className="py-2 px-3 rounded-pill"
                        style={{ backgroundColor: "lightgray" }}
                      >
                        Cheque
                      </span>
                      <span
                        className="py-2 px-3 rounded-pill"
                        style={{ backgroundColor: "lightgray" }}
                      >
                        Cash
                      </span>
                      <span
                        className="py-2 px-3 rounded-pill"
                        style={{ backgroundColor: "lightgray" }}
                      >
                        Cheque
                      </span>
                    </div>
                  </section>
                </div>
                <div className="col-md-6">
                  <section className="p-3">
                    <h4 className="m-0">Practical Information</h4>
                    <p className="text-muted">Restaurant De Bagels | $$</p>
                    <div>icons here</div>
                  </section>

                  <section className="p-3">
                    <h4 className="m-0">A word from the Restaurant</h4>
                    <p className="text-muted">
                      Lorem ipsum text demo tiems smaplle
                    </p>
                  </section>
                  <section className="p-3">
                    <h4 className="m-0">The Restaurant Team</h4>
                    {/* <p className="text-muted">
                    Lorem ipsum text demo tiems smaplle
                  </p> */}
                  </section>
                </div>
              </div>
            </div>
            <div className="d-none d-md-block col-md-2"></div>
          </div>
        </section>
      </RestaurantPageParent>
    </>
  );
};

export default RestaurantInfoPage;

export async function getServerSideProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
