import React from "react";

import RestaurantPageParent from "../../../components/RestaurantPage/RestaurantPageParent";

import styles from "../../../styles/RestaurantPage/RestaurantDiscount.module.scss";

const RestaurantDiscountPage = () => {
  return (
    <>
      <RestaurantPageParent>
        <section id={styles["reduction-section"]} className="container">
          <div className="row">
            <div className="d-none d-md-block col-md-2"></div>
            <div className="col-md-8 my-5">
              <article
                className={`${styles["reduction-card"]} my-4`}
                style={{ backgroundColor: "pink" }}
              >
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
                <div className={styles["card-action"]}>Choose</div>
              </article>
              <article
                className={`${styles["reduction-card"]} my-4`}
                style={{ backgroundColor: "pink" }}
              >
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
                <div className={styles["card-action"]}>Choose</div>
              </article>
              <article
                className={`${styles["reduction-card"]} my-4`}
                style={{ backgroundColor: "pink" }}
              >
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
                <div className={styles["card-action"]}>Choose</div>
              </article>
            </div>
            <div className="d-none d-md-block col-md-2"></div>
          </div>
        </section>
      </RestaurantPageParent>
    </>
  );
};

export default RestaurantDiscountPage;

export async function getServerSideProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
