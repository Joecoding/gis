import React  from "react";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";
import { useRouter } from "next/router";


import AppNav from "../components/shared/AppNav";
import AuthMenu from "../components/shared/AuthMenu";
import AddAddressDialog from "../components/ProfilePage/AddAddressDialog";
import AddCardDialog from "../components/ProfilePage/AddCardDialog";

import Paper from "@mui/material/Paper";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

import { IconButton, TextField } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import Loading from "./loading";
import styles from "../styles/PaymentPage.module.scss";

const NavHeader = ({ t }) => {
    return <h4 className="text-white">{t("payment:appHeader.title")}</h4>;
};



const PaymentPage = () => {
    const { t } = useTranslation("payment");
    const router = useRouter(); 
  

    return (
        <>
            <AppNav header={<NavHeader t={t} />} menu={<AuthMenu />} />
            <main className="container" style={{ paddingTop: "250px" }}>
                <div className="row">
                    <div className="d-none d-md-block col-md-1"></div>
                    <div className="col-md-10">
                        <div className="row">
                            <div
                                className="d-none d-sm-block col-sm-3"
                                style={{
                                    position: "sticky",
                                    top: "0px",
                                    height: "100vh",
                                }}
                            >
                                <section className="my-3">
                                    <Paper
                                        sx={{
                                            borderRadius: "20px",
                                            overflow: "hidden",
                                            // boxShadow: "2px 2px 2px 6px black",
                                        }}
                                        elevation={3}
                                    >
                                        <h4 className="px-3 pt-3 mb-0 text-secondary">
                                            {t(
                                                "profileCommon:sidebar.activity"
                                            )}
                                        </h4>
                                        <div className="py-2">
                                         <Link href="/profile" passHref>
                                            <ListItem
                                                  sx={
                                                    router.pathname === "/profile"
                                                      ? {
                                                          fontWeiht: "700",
                                                          color: "white",
                                                          backgroundColor: "#fe9139",
                                                        }
                                                      : { fontWeight: "700" }
                                                  }
                                                disablePadding
                                            >
                                                <ListItemButton>
                                                    <ListItemText>
                                                        <h6 className="m-0">
                                                            {t(
                                                                "profileCommon:sidebar.modify"
                                                            )}
                                                        </h6>
                                                    </ListItemText>
                                                </ListItemButton>
                                            </ListItem>
                                            </Link>
                                            <Link href="/profile/recently-viewed" passHref>
                                            <ListItem
                                                  sx={
                                                    router.pathname === "/profile/recently-viewed"
                                                      ? {
                                                          fontWeiht: "700",
                                                          color: "white",
                                                          backgroundColor: "#fe9139",
                                                        }
                                                      : { fontWeight: "700" }
                                                  }
                                                disablePadding
                                            >
                                                <ListItemButton>
                                                    <ListItemText>
                                                        <h6 className="m-0">
                                                            {t(
                                                                "profileCommon:sidebar.recently"
                                                            )}
                                                        </h6>
                                                    </ListItemText>
                                                </ListItemButton>
                                            </ListItem>
                                             </Link> 
                                        </div>
                                    </Paper>
                                </section>
                                <section className="my-3">
                                    <Paper
                                        sx={{
                                            borderRadius: "20px",
                                            overflow: "hidden",
                                            // boxShadow: "2px 2px 2px 6px black",
                                        }}
                                        elevation={3}
                                    >
                                        <h4 className="px-3 pt-3 mb-0 text-secondary">
                                            {t(
                                                "profileCommon:sidebar.onlineOrder"
                                            )}
                                        </h4>
                                        <div className="py-2">
                                             <Link href="/profile/history" passHref> 
                                            <ListItem
                                                  sx={
                                                    router.pathname === "/profile/history"
                                                      ? {
                                                          fontWeiht: "700",
                                                          color: "white",
                                                          backgroundColor: "#fe9139",
                                                        }
                                                      : { fontWeight: "700" }
                                                  }
                                                disablePadding
                                            >
                                                <ListItemButton>
                                                    <ListItemText>
                                                        <h6 className="m-0">
                                                            {t(
                                                                "profileCommon:sidebar.history"
                                                            )}
                                                        </h6>
                                                    </ListItemText>
                                                </ListItemButton>
                                            </ListItem>
                                             </Link> 
                                              <Link href="/profile/discounts" passHref> 
                                            <ListItem
                                                  sx={
                                                    router.pathname === "/profile/discounts"
                                                      ? {
                                                          fontWeiht: "700",
                                                          color: "white",
                                                          backgroundColor: "#fe9139",
                                                        }
                                                      : { fontWeight: "700" }
                                                  }
                                                disablePadding
                                            >
                                                <ListItemButton>
                                                    <ListItemText>
                                                        <h6 className="m-0">
                                                            {t(
                                                                "profileCommon:sidebar.discount"
                                                            )}
                                                        </h6>
                                                    </ListItemText>
                                                </ListItemButton>
                                            </ListItem>
                                          </Link> 
                                        </div>
                                    </Paper>
                                </section>
                            </div>

                            <div className="col-md-9">
                                <section className="my-3">
                                    <h4>{t("profileIndex:address.heading")}</h4>
                                    <Paper
                                        sx={{
                                            borderRadius: "20px",
                                            padding: "1.5rem",
                                            marginBottom: "1rem",
                                        }}
                                        elevation={3}
                                    >
                                        <header className="d-flex mb-2">
                                            <h5 className="text-primary fw-bolder flex-grow-1 m-0">
                                                Home
                                            </h5>
                                            <IconButton>
                                                <EditIcon
                                                    sx={{ color: "gray" }}
                                                />
                                            </IconButton>
                                            <IconButton>
                                                <DeleteIcon
                                                    sx={{ color: "gray" }}
                                                />
                                            </IconButton>
                                        </header>
                                        <div>
                                            <h5 className="fw-bolder m-0">
                                                123 street fake address
                                            </h5>
                                            <h5 className="text-muted m-0">
                                                Delhi
                                            </h5>
                                            <h5 className="text-muted m-0">
                                                India
                                            </h5>
                                        </div>
                                    </Paper>
                                    <AddAddressDialog>
                                        <button className="btn-primary">
                                            <div className="d-flex align-items-center gap-1">
                                                <AddCircleOutlineIcon />
                                                {t(
                                                    "profileIndex:address.addAddress"
                                                )}
                                            </div>
                                        </button>
                                    </AddAddressDialog>
                                </section>
                                <section className="my-3">
                                    <h4 className="mb-0">
                                        {t("payment:addressForm.title")}
                                    </h4>
                                    <form className="py-3">
                                        <div className="d-flex flex-column my-2">
                                            <label className="fw-bolder mb-1 mx-3">
                                                {t(
                                                    "payment:addressForm.address"
                                                )}
                                            </label>
                                            <TextField
                                                size="small"
                                                sx={{
                                                    "& .MuiOutlinedInput-root":
                                                        {
                                                            borderRadius:
                                                                "25px",
                                                        },
                                                }}
                                            />
                                        </div>
                                        <div className="d-md-flex align-items-center gap-3 my-2">
                                            <div className="d-flex flex-column flex-grow-1">
                                                <label className="fw-bolder mb-1 mx-3">
                                                    {t(
                                                        "payment:addressForm.address"
                                                    )}
                                                </label>
                                                <TextField
                                                    size="small"
                                                    sx={{
                                                        "& .MuiOutlinedInput-root":
                                                            {
                                                                borderRadius:
                                                                    "25px",
                                                            },
                                                    }}
                                                />
                                            </div>
                                            <div className="d-flex flex-column flex-grow-1">
                                                <label className="fw-bolder mb-1 mx-3">
                                                    {t(
                                                        "payment:addressForm.address"
                                                    )}
                                                </label>
                                                <TextField
                                                    size="small"
                                                    sx={{
                                                        "& .MuiOutlinedInput-root":
                                                            {
                                                                borderRadius:
                                                                    "25px",
                                                            },
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="d-md-flex align-items-center gap-3 my-2">
                                            <div className="d-flex flex-column flex-grow-1">
                                                <label className="fw-bolder mb-1 mx-3">
                                                    {t(
                                                        "payment:addressForm.address"
                                                    )}
                                                </label>
                                                <TextField
                                                    size="small"
                                                    sx={{
                                                        "& .MuiOutlinedInput-root":
                                                            {
                                                                borderRadius:
                                                                    "25px",
                                                            },
                                                    }}
                                                />
                                            </div>
                                            <div className="d-flex flex-column flex-grow-1">
                                                <label className="fw-bolder mb-1 mx-3">
                                                    {t(
                                                        "payment:addressForm.address"
                                                    )}
                                                </label>
                                                <TextField
                                                    size="small"
                                                    sx={{
                                                        "& .MuiOutlinedInput-root":
                                                            {
                                                                borderRadius:
                                                                    "25px",
                                                            },
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div
                                            className="d-flex flex-column my-2 pe-2"
                                            style={{
                                                width: "100%",
                                                maxWidth: "350px",
                                            }}
                                        >
                                            <label className="fw-bolder mb-1 mx-3">
                                                {t(
                                                    "payment:addressForm.address"
                                                )}
                                            </label>
                                            <TextField
                                                size="small"
                                                sx={{
                                                    "& .MuiOutlinedInput-root":
                                                        {
                                                            borderRadius:
                                                                "25px",
                                                        },
                                                }}
                                            />
                                        </div>
                                        <div className="d-flex flex-column my-2">
                                            <label className="fw-bolder mb-1 mx-3">
                                                {t(
                                                    "payment:addressForm.address"
                                                )}
                                            </label>
                                            <TextField
                                                size="small"
                                                multiline
                                                rows={4}
                                                sx={{
                                                    "& .MuiOutlinedInput-root":
                                                        {
                                                            borderRadius:
                                                                "25px",
                                                        },
                                                }}
                                            />
                                        </div>
                                    </form>
                                </section>
                                <section className="my-3">
                                    <h4 className="mb-4">
                                        {t("payment:mode.title")}
                                    </h4>
                                    <div className="d-flex align-items-center flex-wrap-1 gap-2 mb-3">
                                        <div
                                            id={styles["card"]}
                                            className="p-2"
                                            style={{ backgroundColor: "pink" }}
                                        >
                                            <div className="d-flex">
                                                <div className="flex-grow-1">
                                                    <img
                                                        src="/img/mastercard-logo.png"
                                                        alt="mastercart"
                                                        width={50}
                                                    />
                                                </div>
                                                <div>
                                                    <IconButton
                                                        sx={{ padding: "0px" }}
                                                    >
                                                        <EditIcon
                                                            fontSize="small"
                                                            sx={{
                                                                color: "gray",
                                                            }}
                                                        />
                                                    </IconButton>
                                                </div>
                                            </div>
                                            <div className={styles["info"]}>
                                                <p className="fw-bolder text-white m-0">
                                                    **** **** **** 4321
                                                </p>
                                                <p className="fw-bolder text-white m-0">
                                                    {t(
                                                        "profileIndex:payment.expiresOn"
                                                    )}{" "}
                                                    04/12
                                                </p>
                                            </div>
                                        </div>
                                        <div className="flex-grow-1">
                                            <p className="fw-bolder mb-0">
                                                Master Card
                                            </p>
                                            <p className="fw-bolder text-muted mb-0">
                                                **** **** **** 4321
                                                <br />
                                                {t(
                                                    "profileIndex:payment.expiresOn"
                                                )}{" "}
                                                04/12
                                            </p>
                                        </div>
                                        <div>
                                            <h5
                                                className="text-center mb-0"
                                                style={{
                                                    textDecoration: "underline",
                                                    color: "#fe9139",
                                                }}
                                            >
                                                {t("payment:mode.chooseCard")}
                                            </h5>
                                            <p
                                                className="text-center mb-0"
                                                style={{
                                                    textDecoration: "underline",
                                                    color: "gray",
                                                    fontSize: ".7rem",
                                                }}
                                            >
                                                {t("payment:mode.deleteCard")}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="d-flex align-items-center flex-wrap-1 gap-2 mb-3">
                                        <div
                                            id={styles["card"]}
                                            className="p-2"
                                            style={{ backgroundColor: "pink" }}
                                        >
                                            <div className="d-flex">
                                                <div className="flex-grow-1">
                                                    <img
                                                        src="/img/mastercard-logo.png"
                                                        alt="mastercart"
                                                        width={50}
                                                    />
                                                </div>
                                                <div>
                                                    <IconButton
                                                        sx={{ padding: "0px" }}
                                                    >
                                                        <EditIcon
                                                            fontSize="small"
                                                            sx={{
                                                                color: "gray",
                                                            }}
                                                        />
                                                    </IconButton>
                                                </div>
                                            </div>
                                            <div className={styles["info"]}>
                                                <p className="fw-bolder text-white m-0">
                                                    **** **** **** 4321
                                                </p>
                                                <p className="fw-bolder text-white m-0">
                                                    {t(
                                                        "profileIndex:payment.expiresOn"
                                                    )}{" "}
                                                    04/12
                                                </p>
                                            </div>
                                        </div>
                                        <div className="flex-grow-1">
                                            <p className="fw-bolder mb-0">
                                                Master Card
                                            </p>
                                            <p className="fw-bolder text-muted mb-0">
                                                **** **** **** 4321
                                                <br />
                                                {t(
                                                    "profileIndex:payment.expiresOn"
                                                )}{" "}
                                                04/12
                                            </p>
                                        </div>
                                        <div>
                                            <h5
                                                className="text-center pointer mb-0"
                                                style={{
                                                    textDecoration: "underline",
                                                    color: "#fe9139",
                                                }}
                                            >
                                                {t("payment:mode.chooseCard")}
                                            </h5>
                                            <p
                                                className="text-center pointer mb-0"
                                                style={{
                                                    textDecoration: "underline",
                                                    color: "gray",
                                                    fontSize: ".7rem",
                                                }}
                                            >
                                                {t("payment:mode.deleteCard")}
                                            </p>
                                        </div>
                                    </div>
                                    <AddCardDialog t={t}>
                                        <button className="btn-primary">
                                            <div className="d-flex align-items-center gap-1">
                                                <AddCircleOutlineIcon />
                                                {t(
                                                    "profileIndex:payment.addCard"
                                                )}
                                            </div>
                                        </button>
                                    </AddCardDialog>
                                </section>
                                <section className="d-flex justify-content-center my-5 py-5">
                                   <Link href="/loading" passHref>
                                    <button
                                        className="btn-primary px-5"
                                        style={{ fontSize: "1.2rem" }}
                                        
                                    >
                                        Payer
                                    </button>
                                    </Link>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div className="d-none d-md-block col-md-1"></div>
                </div>
            </main>
        </>
    );
};

export default PaymentPage;
