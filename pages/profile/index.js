import React, { useState } from "react";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";

import AddAddressDialog from "../../components/ProfilePage/AddAddressDialog";
import AddCardDialog from "../../components/ProfilePage/AddCardDialog";
import ChangePassDialog from "../../components/ProfilePage/ChangePassDialog";
import EditInfoDialog from "../../components/ProfilePage/EditInfoDialog";

import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import Switch from "@mui/material/Switch";

import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import FacebookIcon from "@mui/icons-material/Facebook";
import GoogleIcon from "@mui/icons-material/Google";

import styles from "./../../styles/ProfilePage/ProfilePage.module.scss";
import ProfilePageLayout from "../../components/ProfilePage/ProfilePageLayout";

const ProfilePage = () => {
  const { t } = useTranslation("profileIndex");

  const [changePassOpen, setChangePassOpen] = useState(false);

  const handleChangePassOpen = () => {
    setChangePassOpen(true);
  };
  const handleChangePassClose = () => {
    setChangePassOpen(false);
  };

  return (
    <>
      <ProfilePageLayout>
        <section className="my-3">
          <h4>{t("profileIndex:infoSec.heading")}</h4>
          <Paper
            elevation={3}
            sx={{
              borderRadius: "20px",
              padding: "1.5rem",
            }}
          >
            <div className="d-flex gap-3 mb-3">
              <div className="d-flex align-items-center gap-3 flex-grow-1">
                <img
                  src="/img/bagel-cover.jpg"
                  alt="profile-img"
                  width={80}
                  height={80}
                  className="rounded-circle"
                />
                <div>
                  <h5 className="m-0 text-primary fw-bolder">
                    {t("profileIndex:infoSec.status")}
                  </h5>
                  <small
                    className="text-muted"
                    // style={{ lineHeight: "0.8em" }}
                  >
                    {t("profileIndex:infoSec.statusInfo")}
                  </small>
                </div>
              </div>
              <EditInfoDialog>
              <div>
                <IconButton>
                  <EditIcon sx={{ color: "gray" }} />
                </IconButton>
              </div>
              </EditInfoDialog>
            </div>
            <div className="row">
              <div className="col-sm-6 px-sm-3 pb-sm-3">
                <h5 className="fw-bolder m-0">
                  {t("profileIndex:infoSec.lname")}
                </h5>
                <h5 className="m-0">Bernard</h5>
              </div>

              <div className="col-sm-6 px-sm-3 pb-sm-3">
                <h5 className="fw-bolder m-0">
                  {t("profileIndex:infoSec.fname")}
                </h5>
                <h5 className="m-0">Bernard</h5>
              </div>
              <div className="col-sm-6 px-sm-3">
                <h5 className="fw-bolder m-0">
                  {t("profileIndex:infoSec.mobile")}
                </h5>
                <h5 className="m-0">Bernard</h5>
              </div>
              <div className="col-sm-6 px-sm-3">
                <h5 className="fw-bolder m-0">
                  {t("profileIndex:infoSec.email")}
                </h5>
                <h5 className="m-0">Bernard</h5>
              </div>
            </div>
          </Paper>
        </section>
        <section className="my-3">
          <h4>{t("profileIndex:address.heading")}</h4>
          <Paper
            sx={{
              borderRadius: "20px",
              padding: "1.5rem",
              marginBottom: "1rem",
            }}
            elevation={3}
          >
            <header className="d-flex mb-2">
              <h5 className="text-primary fw-bolder flex-grow-1 m-0">Home</h5>
              <IconButton>
                <EditIcon sx={{ color: "gray" }} />
              </IconButton>
              <IconButton>
                <DeleteIcon sx={{ color: "gray" }} />
              </IconButton>
            </header>
            <div>
              <h5 className="fw-bolder m-0">123 street fake address</h5>
              <h5 className="text-muted m-0">Delhi</h5>
              <h5 className="text-muted m-0">India</h5>
            </div>
          </Paper>
          <AddAddressDialog>
            <button className="btn-primary">
              <div className="d-flex align-items-center gap-1">
                <AddCircleOutlineIcon />
                {t("profileIndex:address.addAddress")}
              </div>
            </button>
          </AddAddressDialog>
        </section>
        <section className="my-3">
          <h4>{t("profileIndex:payment.heading")}</h4>
           {[...Array(2)].map((i) => {
              return(
                <div
                id={styles["card"]}
                className="p-4 mb-3"
                style={{ backgroundColor: "pink" }}
                key={i}
              >
                <div className="d-flex">
                  <div className="flex-grow-1">
                    <img
                      src="/img/mastercard-logo.png"
                      alt="mastercart"
                      width={100}
                    />
                  </div>
                  <div>
                    <IconButton>
                      <EditIcon sx={{ color: "gray" }} />
                    </IconButton>
                    <IconButton>
                      <DeleteIcon sx={{ color: "gray" }} />
                    </IconButton>
                  </div>
                </div>
                <div className={styles["info"]}>
                  <h5 className="fw-bolder text-white m-0">**** **** **** 4321</h5>
                  <h5 className="fw-bolder text-white m-0">
                    {t("profileIndex:payment.expiresOn")} 04/12
                  </h5>
                </div>
              </div>
              );
           })}
          <AddCardDialog>
            <button className="btn-primary">
              <div className="d-flex align-items-center gap-1">
                <AddCircleOutlineIcon />
                {t("profileIndex:payment.addCard")}
              </div>
            </button>
          </AddCardDialog>
        </section>
        <section className="my-3">
          <h4>{t("profileIndex:password.heading")}</h4>
          <Paper elevation={3} sx={{ borderRadius: "20px", padding: "1.5rem" }}>
            <p className="text-center mb-4">
              {t("profileIndex:password.description")}
            </p>
            <form
              className="text-center"
              onSubmit={(e) => {
                e.preventDefault();
                handleChangePassOpen();
              }}
            >
              <div>
                <label>{t("profileIndex:password.formLabel")}</label>
              </div>
              <input
                type="text"
                placeholder="Enter Password"
                required
                style={{
                  border: "1px solid lightgray",
                  fontSize: "0.8rem",
                  padding: "0.7rem 1rem",
                  borderRadius: "8px",
                  margin: "10px 0px",
                }}
              />
              <div className="d-flex justify-content-center">
                <ChangePassDialog
                  open={changePassOpen}
                  handleClickOpen={handleChangePassOpen}
                  handleClose={handleChangePassClose}
                >
                  <button className="btn-secondary" type="submit">
                    {t("profileIndex:password.modifyBtn")}
                  </button>
                </ChangePassDialog>
              </div>
            </form>
          </Paper>
        </section>
        <section className="my-3">
          <h4>{t("profileIndex:social.heading")}</h4>
          <Paper elevation={3} sx={{ borderRadius: "20px", padding: "1.5rem" }}>
            <div className="d-flex align-items-center my-3">
              <div className="d-flex align-items-center gap-3 flex-grow-1">
                <GoogleIcon />
                <h5 className="fw-bolder mb-0">Google</h5>
              </div>
              <div>
                <Link href="/#">Sign Out</Link>
              </div>
            </div>
            <div className="d-flex align-items-center my-3">
              <div className="d-flex align-items-center gap-3 flex-grow-1">
                <FacebookIcon />
                <h5 className="fw-bolder mb-0">Facebook</h5>
              </div>
              <div>
                <Link href="/#">Sign Out</Link>
              </div>
            </div>
            <div className="text-center">
              <small>{t("profileIndex:social.description")}</small>
            </div>
          </Paper>
        </section>
        <section className="my-3">
          <h4>{t("profileIndex:newsletter.heading")}</h4>
          <Paper
            elevation={3}
            sx={{ borderRadius: "20px", padding: "2.5rem 1.5rem" }}
          >
            <div className="d-flex align-items-center justify-content-center gap-5">
              <Switch defaultChecked />
              <p className="m-0">{t("profileIndex:newsletter.description")}</p>
            </div>
          </Paper>
        </section>
        <section className="my-3">
          <h4>{t("profileIndex:delete.heading")}</h4>
          <Paper elevation={3} sx={{ borderRadius: "20px", padding: "1.5rem" }}>
            <p className="text-center mb-4">
              {t("profileIndex:delete.description")}
            </p>
            <form className="text-center">
              <div>
                <label>{t("profileIndex:delete.formLabel")}</label>
              </div>
              <input
                type="text"
                placeholder="Enter Password"
                style={{
                  border: "1px solid lightgray",
                  fontSize: "0.8rem",
                  padding: "0.7rem 1rem",
                  borderRadius: "8px",
                  margin: "10px 0px",
                }}
              />
              <div className="d-flex justify-content-center">
                <button className="btn-secondary">
                  {t("profileIndex:delete.modifyBtn")}
                </button>
              </div>
            </form>
          </Paper>
        </section>
      </ProfilePageLayout>
    </>
  );
};

export default ProfilePage;
