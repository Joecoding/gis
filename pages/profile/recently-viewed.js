import React from "react";
import useTranslation from "next-translate/useTranslation";

import ProfilePageLayout from "../../components/ProfilePage/ProfilePageLayout";

const RecentlyViewedPage = ({}) => {
  const { t } = useTranslation("recentlyViewed");
  return (
    <>
      <ProfilePageLayout>
        <section className="py-3">
          <h4 className="mb-3">{t("recentlyViewed:title")}</h4>
          <div className="row">
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
            <div className="col-sm-6 mt-4">
              <h5>Bagel Corner</h5>
              <img
                src="/img/bagel-cover.jpg"
                alt="Restaurant"
                style={{ borderRadius: "30px", width: "100%" }}
              />
            </div>
          </div>
        </section>
      </ProfilePageLayout>
    </>
  );
};

export default RecentlyViewedPage;
