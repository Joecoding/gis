import React, { useState } from "react";

import useTranslation from "next-translate/useTranslation";

import ProfilePageLayout from "../../components/ProfilePage/ProfilePageLayout";

import styles from "../../styles/ProfilePage/DiscountPage.module.scss";
import UseDiscountDialog from "../../components/ProfilePage/UseDiscountDialog";

const ProfileDiscountPage = () => {
  const { t } = useTranslation("discount");
  const [currentView, setCurrentView] = useState("valid");

  return (
    <>
      <ProfilePageLayout>
        <nav
          id={styles["page-nav"]}
          className="mt-3 mb-4 d-flex align-items-center justify-content-center gap-3"
        >
          <div
            className={`${styles.tab} ${
              currentView === "valid" ? styles["active"] : ""
            }`}
            onClick={() => setCurrentView("valid")}
          >
            {t("discount:tabs.valid")}
          </div>
          <div
            className={`${styles.tab} ${
              currentView === "toUnlock" ? styles["active"] : ""
            }`}
            onClick={() => setCurrentView("toUnlock")}
          >
            {t("discount:tabs.toUnlock")}
          </div>
        </nav>
        {currentView === "valid" ? (
          <>
            <section id={styles["valid"]}>
              <article
                className={`${styles["reduction-card"]} my-4 py-3 px-4`}
                style={{ backgroundColor: "pink" }}
              >
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
              </article>
              <article
                className={`${styles["reduction-card"]} my-4 py-3 px-4`}
                style={{ backgroundColor: "pink" }}
              >
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
              </article>
              <article
                className={`${styles["reduction-card"]} my-4 py-3 px-4`}
                style={{ backgroundColor: "pink" }}
              >
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
              </article>
            </section>
          </>
        ) : currentView === "toUnlock" ? (
          <>
            <section id={styles["to-unlock"]}>
              <article className={`${styles["reduction-card"]} my-4 py-3 px-4`}>
                <UseDiscountDialog>
                  <button className={`${styles["card-action"]} btn-primary`}>
                    100 geeds
                  </button>
                </UseDiscountDialog>
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
              </article>
              <article className={`${styles["reduction-card"]} my-4 py-3 px-4`}>
                <button className={`${styles["card-action"]} btn-primary`}>
                  100 geeds
                </button>
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
              </article>
              <article className={`${styles["reduction-card"]} my-4 py-3 px-4`}>
                <button className={`${styles["card-action"]} btn-primary`}>
                  100 geeds
                </button>
                <div className={styles["card-content"]}>
                  <div className={styles["card-content-head"]}>
                    <h2 className="m-0">- 20%</h2>
                    <small>Until Jan 31 2021</small>
                  </div>
                  <div className={styles["card-content-body"]}>
                    <h4 className="m-0">All over the bagel corner</h4>
                  </div>
                </div>
              </article>
            </section>
          </>
        ) : (
          ""
        )}
      </ProfilePageLayout>
    </>
  );
};

export default ProfileDiscountPage;
