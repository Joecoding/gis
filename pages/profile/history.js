import React, { useState } from "react";
import Link from "next/link";

import useTranslation from "next-translate/useTranslation";

import ProfilePageLayout from "../../components/ProfilePage/ProfilePageLayout";
import WriteReviewDialog from "../../components/ProfilePage/WriteReviewDialog";

import { Paper } from "@mui/material";

import styles from "../../styles/ProfilePage/OrderHistoryPage.module.scss";

const ProfileHistoryPage = () => {
  const { t } = useTranslation("history");
  const [currentView, setCurrentView] = useState("inProgress");

  return (
    <>
      <ProfilePageLayout>
        <nav
          id={styles["page-nav"]}
          className="mt-3 mb-4 d-flex align-items-center justify-content-center gap-3"
        >
          <div
            className={`${styles.tab} ${
              currentView === "inProgress" ? styles["active"] : ""
            }`}
            onClick={() => setCurrentView("inProgress")}
          >
            {t("history:tabs.inProgress")}
          </div>
          <div
            className={`${styles.tab} ${
              currentView === "past" ? styles["active"] : ""
            }`}
            onClick={() => setCurrentView("past")}
          >
            {t("history:tabs.past")}
          </div>
          <div
            className={`${styles.tab} ${
              currentView === "toRepay" ? styles["active"] : ""
            }`}
            onClick={() => setCurrentView("toRepay")}
          >
            {t("history:tabs.toRepay")}
          </div>
        </nav>
        {currentView === "inProgress" ? (
          <>
            <section className="my-3">
              <div className="row">
                <div className="col-sm-6">
                  <Paper
                    elevation={3}
                    sx={{
                      padding: "1.5rem",
                      borderRadius: "20px",
                      marginTop: "10px",
                      position: "relative",
                    }}
                  >
                    <div
                      className={`${styles["order-payment-status"]} ${styles["prepaid"]}`}
                    >
                      {t("history:inProgress.prepaid")}
                    </div>
                    <article>
                      <h5 className="fw-bolder mb-0">
                        {t("history:inProgress.order")} #456
                      </h5>
                      <small className="text-muted">12 Jun 2021</small>
                    </article>
                    <hr />
                    <article>
                      <p className="mb-0">
                        {t("history:inProgress.bookingDate")}
                      </p>
                      <p className="mb-0">
                        {t("history:inProgress.bookingPerson")}
                      </p>
                      <p className="mb-0 my-2">
                        {t("history:inProgress.orderPrice")}
                      </p>
                      <div className="d-flex flex-row-reverse mt-3">
                        <Link href="/#">
                          <small className="text-muted pointer">
                            {t("history:inProgress.orderDetail")}
                          </small>
                        </Link>
                      </div>
                    </article>
                  </Paper>
                </div>
              </div>
            </section>
          </>
        ) : currentView === "past" ? (
          <>
            <table id={styles["past-order-table"]}>
              <thead>
                <tr>
                  <th scope="col">{t("history:past.orderNum")}</th>
                  <th scope="col">{t("history:past.date")}</th>
                  <th scope="col">{t("history:past.status")}</th>
                  <th scope="col">{t("history:past.deliveryDate")}</th>
                  <th scope="col">{t("history:past.price")}</th>
                </tr>
              </thead>
              <tbody>
                <tr className={`${styles["table-row"]} `}>
                  <td data-label={t("history:past.orderNum")}>
                    <div className="text-lg-center">
                      <p className="fw-bolder mb-0">#453</p>
                      <WriteReviewDialog>
                        <a className="mb-0 pointer">
                          {t("history:past.writeReview")}
                        </a>
                      </WriteReviewDialog>
                    </div>
                  </td>
                  <td data-label={t("history:past.date")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.status")}>
                    {t("history:past.recovered")}
                  </td>
                  <td data-label={t("history:past.deliveryDate")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.price")}>$ 45.00</td>
                </tr>
                <tr className={`${styles["table-row"]} `}>
                  <td data-label={t("history:past.orderNum")}>
                    <div className="text-lg-center">
                      <p className="fw-bolder mb-0">#453</p>
                      <p className="mb-0">{t("history:past.writeReview")}</p>
                    </div>
                  </td>
                  <td data-label={t("history:past.date")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.status")}>
                    {t("history:past.recovered")}
                  </td>
                  <td data-label={t("history:past.deliveryDate")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.price")}>$ 45.00</td>
                </tr>
                <tr className={`${styles["table-row"]} `}>
                  <td data-label={t("history:past.orderNum")}>
                    <div className="text-lg-center">
                      <p className="fw-bolder mb-0">#453</p>
                      <p className="mb-0">{t("history:past.writeReview")}</p>
                    </div>
                  </td>
                  <td data-label={t("history:past.date")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.status")}>
                    {t("history:past.recovered")}
                  </td>
                  <td data-label={t("history:past.deliveryDate")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.price")}>$ 45.00</td>
                </tr>
                <tr className={`${styles["table-row"]} `}>
                  <td data-label={t("history:past.orderNum")}>
                    <div className="text-lg-center">
                      <p className="fw-bolder mb-0">#453</p>
                      <p className="mb-0">{t("history:past.writeReview")}</p>
                    </div>
                  </td>
                  <td data-label={t("history:past.date")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.status")}>
                    {t("history:past.recovered")}
                  </td>
                  <td data-label={t("history:past.deliveryDate")}>
                    04 Jun 2021, 13:02
                  </td>
                  <td data-label={t("history:past.price")}>$ 45.00</td>
                </tr>
              </tbody>
            </table>
          </>
        ) : currentView === "toRepay" ? (
          <>
            <section className="my-3">
              <div className="row">
                <div className="col-sm-6">
                  <Paper
                    elevation={3}
                    sx={{
                      padding: "1.5rem",
                      borderRadius: "20px",
                      marginTop: "10px",
                      position: "relative",
                    }}
                  >
                    <div
                      className={`${styles["order-payment-status"]} ${styles["repay"]}`}
                    >
                      {t("history:tabs.toRepay")}
                    </div>
                    <article>
                      <h5 className="fw-bolder mb-0">
                        {t("history:inProgress.order")} #456
                      </h5>
                      <small className="text-muted">12 Jun 2021</small>
                    </article>
                    <hr />
                    <article>
                      <p className="mb-0">
                        {t("history:inProgress.bookingDate")}
                      </p>
                      <p className="mb-0">
                        {t("history:inProgress.bookingPerson")}
                      </p>
                      <p className="mb-0 my-2">
                        {t("history:inProgress.orderPrice")}
                      </p>
                    </article>
                    <div className={styles["order-card-action"]}>
                      <button className="btn-primary">
                        {t("history:toRepay.reimburse")}
                      </button>
                    </div>
                  </Paper>
                </div>
              </div>
            </section>
          </>
        ) : (
          ""
        )}
      </ProfilePageLayout>
    </>
  );
};

export default ProfileHistoryPage;
