import React, { useState , useEffect } from "react";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";
import { useRouter } from "next/router";


import AppNav from "/components/shared/AppNav";
import AuthMenu from "/components/shared/AuthMenu";
import AddAddressDialog from "/components/ProfilePage/AddAddressDialog";
import AddCardDialog from "/components/ProfilePage/AddCardDialog";

import Paper from "@mui/material/Paper";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

import { IconButton, TextField } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";

import styles from "../styles/PaymentPage.module.scss";

const NavHeader = ({ t }) => {
    return <h4 className="text-white">{t("payment:appProcessing.title")}</h4>;
};

const Loading = ()  => {
    const { t } = useTranslation("payment-processing");
       
    return(

        <>

        <AppNav header={<NavHeader t={t} />} menu={<AuthMenu />} />
         
        <div
        style={{
            backgroundImage: "url(/img/bagel-cover.jpg)",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: "450px",
            backgroundPosition: "center center",
        }}
      ></div>

       </>

        
     );
       
}


export default Loading;