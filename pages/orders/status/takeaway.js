import { useState } from "react";
import Link from "next/link";

import AppNav from "../../../components/shared/AppNav";
import AuthMenu from "../../../components/shared/AuthMenu";
import OrderStepper from "../../../components/shared/OrderStepper";

import { LinearProgress } from "@mui/material";

import PhoneIcon from "@mui/icons-material/Phone";

import styles from "../../../styles/Order/StatusPages/Delivery.module.scss";

const NavHeader = () => {
  return <h4 className="text-white"></h4>;
};

const ConfimationState = ({ totalSteps, refused }) => {
  if (refused === true) {
    return (
      <>
        <section className="my-5 mx-3">
          <OrderStepper
            totalSteps={totalSteps}
            currentStep={1}
            refused={refused}
          />
        </section>
        <section className="my-5">
          <div className="d-flex justify-content-center">
            <img
              src="/img/food-combo.png"
              alt="food-image"
              className={`${styles["food-banner"]} px-3`}
            />
          </div>
        </section>
        <section id="order-status" className="container  my-5">
          <article className="d-flex flex-column align-items-center justify-content-center">
            <h4
              className="mb-3 text-uppercase text-center"
              style={{ maxWidth: "700px" }}
            >
              Votre commande a ete refusee.
            </h4>
            <h4
              className="mb-5 pb-5 text-uppercase text-center"
              style={{ maxWidth: "700px" }}
            >
              Vous serez automatiquement rembourse apres quelques jours sur
              votre moyen de paiement
            </h4>
          </article>
          <article className="text-center">
            <Link href="/#" passHref>
              <a className="text-muted">Aide</a>
            </Link>
          </article>
        </section>
      </>
    );
  }
  return (
    <>
      <section className="my-5 mx-3">
        <OrderStepper totalSteps={totalSteps} currentStep={1} />
      </section>
      <section className="my-5">
        <div className="d-flex justify-content-center">
          <img
            src="/img/food-combo.png"
            alt="food-image"
            className={`${styles["food-banner"]} px-3`}
          />
        </div>
      </section>
      <section
        id={styles["donation-section"]}
        className="container d-flex flex-sm-row gap-5 flex-column align-items-center justify-content-center my-5"
      >
        <div>
          <div className={`${styles["price"]}`}>
            <h1 className={`mb-0`}>
              0.75<span className={styles["currency"]}>$</span>
            </h1>
          </div>
        </div>
        <div className={`${styles["body"]} mx-3 mx-sm-0`}>
          <h4>Merci pour votre geste !</h4>
          <p>
            <strong>Votre micro-don + 10%</strong> de notre benefice sur votre
            commande seront reverses dans la cagnotte du restaurant pour offrir
            un repan complet a une <strong>personne dans le besoin</strong>
          </p>
          <p className="text-orange fw-bolder mb-0">
            #Heretofighthunger #zerohunger #zerogaspi #Feedthecity
          </p>
        </div>
      </section>
      <section id="order-status" className="container  my-5">
        <article className="d-flex align-items-center justify-content-center">
          <h4
            className="py-5 text-uppercase text-center"
            style={{ maxWidth: "700px" }}
          >
            Merci votre commande a bien ete passee en attente de l'acceptation
            de <span className="text-orange">l'estaminet</span>
          </h4>
        </article>
        <article>
          <div className="text-center">
            <p className="mb-1">Date de livraison estimee:</p>
            <h5 className="fw-bolder">01/10/2021 a 20:30</h5>
          </div>
        </article>
        <article className="my-5 text-center">
          <Link href="/order-recap" passHref>
            <a className="text-muted">voir le detail de la commande</a>
          </Link>
        </article>
      </section>
    </>
  );
};

const PreparationState = ({ totalSteps, refused }) => {
  if (refused === true) {
    return (
      <>
        <section className="my-5 mx-3">
          <OrderStepper
            totalSteps={totalSteps}
            currentStep={1}
            refused={refused}
          />
        </section>
        <section className="my-5">
          <div className="d-flex justify-content-center">
            <img
              src="/img/food-combo.png"
              alt="food-image"
              className={`${styles["food-banner"]} px-3`}
            />
          </div>
        </section>
        <section id="order-status" className="container  my-5">
          <article className="d-flex flex-column align-items-center justify-content-center">
            <h4
              className="mb-3 text-uppercase text-center"
              style={{ maxWidth: "700px" }}
            >
              Votre commande a ete refusee.
            </h4>
            <h4
              className="mb-5 pb-5 text-uppercase text-center"
              style={{ maxWidth: "700px" }}
            >
              Vous serez automatiquement rembourse apres quelques jours sur
              votre moyen de paiement
            </h4>
          </article>
          <article className="text-center">
            <Link href="/#" passHref>
              <a className="text-muted">Aide</a>
            </Link>
          </article>
        </section>
      </>
    );
  }
  return (
    <>
      <section className="my-5 mx-3">
        <OrderStepper totalSteps={totalSteps} currentStep={2} />
      </section>
      <section className="my-5">
        <div className="d-flex justify-content-center">
          <img
            src="/img/food-combo.png"
            alt="food-image"
            className={`${styles["food-banner"]} px-3`}
          />
        </div>
      </section>
      <section
        id={styles["donation-section"]}
        className="container d-flex flex-sm-row gap-5 flex-column align-items-center justify-content-center my-5"
      >
        <div>
          <div className={`${styles["price"]}`}>
            <h1 className={`mb-0`}>
              0.75<span className={styles["currency"]}>$</span>
            </h1>
          </div>
        </div>
        <div className={`${styles["body"]} mx-3 mx-sm-0`}>
          <h4>Merci pour votre geste !</h4>
          <p>
            <strong>Votre micro-don + 10%</strong> de notre benefice sur votre
            commande seront reverses dans la cagnotte du restaurant pour offrir
            un repan complet a une <strong>personne dans le besoin</strong>
          </p>
          <p className="text-orange fw-bolder mb-0">
            #Heretofighthunger #zerohunger #zerogaspi #Feedthecity
          </p>
        </div>
      </section>
      <section id="order-status" className="container  my-5">
        <article className="d-flex align-items-center justify-content-center">
          <h4
            className="py-5 text-uppercase text-center"
            style={{ maxWidth: "700px" }}
          >
            Votre commande a ete acceptee par{" "}
            <span className="text-orange">l'estaminet</span> et est en cours de
            preparation
          </h4>
        </article>
        <article>
          <div className="text-center">
            <p className="mb-1">Date de livraison estimee:</p>
            <h5 className="fw-bolder">01/10/2021 a 20:30</h5>
          </div>
        </article>
        <article className="my-5 text-center">
          <Link href="/order-recap" passHref>
            <a className="text-muted">voir le detail de la commande</a>
          </Link>
        </article>
      </section>
    </>
  );
};

const DeliveryState = ({ totalSteps, refused }) => {
  const [totalProgress] = useState(10);

  if (refused === true) {
    return (
      <>
        <section className="my-5 mx-3">
          <OrderStepper
            totalSteps={totalSteps}
            currentStep={1}
            refused={refused}
          />
        </section>
        <section className="my-5">
          <div className="d-flex justify-content-center">
            <img
              src="/img/food-combo.png"
              alt="food-image"
              className={`${styles["food-banner"]} px-3`}
            />
          </div>
        </section>
        <section id="order-status" className="container  my-5">
          <article className="d-flex flex-column align-items-center justify-content-center">
            <h4
              className="mb-3 text-uppercase text-center"
              style={{ maxWidth: "700px" }}
            >
              Votre commande a ete refusee.
            </h4>
            <h4
              className="mb-5 pb-5 text-uppercase text-center"
              style={{ maxWidth: "700px" }}
            >
              Vous serez automatiquement rembourse apres quelques jours sur
              votre moyen de paiement
            </h4>
          </article>
          <article className="text-center">
            <Link href="/#" passHref>
              <a className="text-muted">Aide</a>
            </Link>
          </article>
        </section>
      </>
    );
  }

  return (
    <>
      <section className="my-5 mx-3">
        <OrderStepper totalSteps={totalSteps} currentStep={3} />
      </section>
      <section className="my-5">
        <div className="d-flex justify-content-center">
          <img
            src="/img/food-combo.png"
            alt="food-image"
            className={`${styles["food-banner"]} px-3`}
          />
        </div>
      </section>
      <section id={styles["tracker"]} className="container my-5 py-5">
        <div className="d-flex align-items-end">
          <div className={styles["place-tag"]}>
            <img src="/img/icons/restaurant.png" />
          </div>
          <div className={`${styles["tracker-line"]} flex-grow-1 w-100`}>
            <div
              className={styles["bicycle"]}
              style={{
                left: totalProgress + "%",
              }}
            >
              <img src="/img/bicycle.png" alt="bicycle" width={70} />
            </div>
            <div>
              <LinearProgress
                variant="determinate"
                value={totalProgress}
                sx={{
                  backgroundColor: "lightgray",
                  "& .MuiLinearProgress-bar ": { backgroundColor: "#700cbe" },
                }}
              />
            </div>
          </div>
          <div style={{ position: "relative" }}>
            <div className={`${styles["place-tag"]}`}>
              <img src="/img/icons/home.png" />
            </div>
            <div className={`${styles["time-left"]} text-center`}>
              <h5>arrivee prevue a</h5>
              <h4 className="fw-bolder">20:30</h4>
            </div>
          </div>
        </div>
      </section>
      <section id="order-status" className="container  my-5">
        <article className="d-flex align-items-center justify-content-center">
          <h4
            className="py-5 text-uppercase text-center"
            style={{ maxWidth: "700px" }}
          >
            votre commande est prete et va etre confiee a un coursier
          </h4>
        </article>
        <article>
          <div className="text-center">
            <p className="text-muted fw-bolder">
              Commande prise en charge par:{" "}
              <span className="text-orange">Sidi</span>
            </p>
            <div className="d-flex align-items-end justify-content-center gap-2 mt-3">
              <PhoneIcon sx={{ fontSize: "40px" }} />
              <a className="text-muted pointer">
                <h5 className="mb-0">12.324.234.12</h5>
              </a>
            </div>
          </div>
        </article>
        <article className="my-5 text-center">
          <Link href="/order-recap" passHref>
            <a className="text-muted">voir le detail de la commande</a>
          </Link>
        </article>
      </section>
    </>
  );
};

const TakeawayStatusPage = () => {
  const [orderStatus] = useState("delivery");

  const [totalSteps] = useState(3);

  return (
    <>
      <AppNav header={<NavHeader />} menu={<AuthMenu />}></AppNav>
      <div
        style={{
          backgroundImage: "url(/img/bagel-cover.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "450px",
          backgroundPosition: "center center",
        }}
      ></div>
      <main>
        {orderStatus === "confirmation" ? (
          <ConfimationState totalSteps={totalSteps} refused={false} />
        ) : orderStatus === "preparation" ? (
          <PreparationState totalSteps={totalSteps} refused={false} />
        ) : orderStatus === "delivery" ? (
          <DeliveryState totalSteps={totalSteps} refused={false} />
        ) : (
          ""
        )}
      </main>
    </>
  );
};

export default TakeawayStatusPage;

export async function getStaticProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
