import React from "react";
import useTranslation from "next-translate/useTranslation";

import AppNav from "../../components/shared/AppNav";
import AuthMenu from "../../components/shared/AuthMenu";

import Checkbox from "@mui/material/Checkbox";

import DownloadIcon from "@mui/icons-material/Download";

import styles from "../../styles/Order/RecapPage.module.scss";
import { Paper } from "@mui/material";

const NavHeader = ({ t }) => {
  return <h4 className="text-white">{t("orderRecap:appHeader.title")}</h4>;
};

const OrderRacap = () => {
  const { t } = useTranslation("orderRecap");

  return (
    <>
      <AppNav header={<NavHeader t={t} />} menu={<AuthMenu />} />
      <main className="container" style={{ paddingTop: "250px" }}>
        <div className="row">
          <div className="d-none d-md-block col-md-2"></div>
          <div className="col-md-8">
            <section id={styles["order-items"]} className="my-5">
              <article className={`${styles["item"]} d-flex gap-3`}>
                <div className={`${styles["img-price"]} d-flex`}>
                  <img
                    src="/img/bagel-cover.jpg"
                    alt="img"
                    style={{ borderRadius: "30px", width: "240px" }}
                  />
                  <p className={`${styles["price-tag"]} mb-0`}>$ 12.50</p>
                </div>
                <div className="flex-grow-1 d-flex flex-column justify-content-center">
                  <h4>Creamy Bagel</h4>
                  <p className="mb-0">
                    Cream cheese, assortiments de fruits au choix
                  </p>
                  <p className="mb-0">x1</p>
                </div>
              </article>
              <article className={`${styles["item"]} d-flex gap-3`}>
                <div className={`${styles["img-price"]} d-flex`}>
                  <img
                    src="/img/bagel-cover.jpg"
                    alt="img"
                    style={{ borderRadius: "30px", width: "240px" }}
                  />
                  <p className={`${styles["price-tag"]} mb-0`}>$ 12.50</p>
                </div>
                <div className="flex-grow-1 d-flex flex-column justify-content-center">
                  <h4>Creamy Bagel</h4>
                  <p className="mb-0">
                    Cream cheese, assortiments de fruits au choix
                  </p>
                  <p className="mb-0">x1</p>
                </div>
              </article>
              <article className={`${styles["item"]} d-flex gap-3`}>
                <div className={`${styles["img-price"]} d-flex`}>
                  <img
                    src="/img/bagel-cover.jpg"
                    alt="img"
                    style={{ borderRadius: "30px", width: "240px" }}
                  />
                  <p className={`${styles["price-tag"]} mb-0`}>$ 12.50</p>
                </div>
                <div className="flex-grow-1 d-flex flex-column justify-content-center">
                  <h4>Creamy Bagel</h4>
                  <p className="mb-0">
                    Cream cheese, assortiments de fruits au choix
                  </p>
                  <p className="mb-0">x1</p>
                </div>
              </article>
              <article className={`${styles["item"]} d-flex gap-3`}>
                <div className={`${styles["img-price"]} d-flex`}>
                  <img
                    src="/img/bagel-cover.jpg"
                    alt="img"
                    style={{ borderRadius: "30px", width: "240px" }}
                  />
                  <p className={`${styles["price-tag"]} mb-0`}>$ 12.50</p>
                </div>
                <div className="flex-grow-1 d-flex flex-column justify-content-center">
                  <h4>Creamy Bagel</h4>
                  <p className="mb-0">
                    Cream cheese, assortiments de fruits au choix
                  </p>
                  <p className="mb-0">x1</p>
                </div>
              </article>
              <article className={`${styles["item"]} d-flex gap-3`}>
                <div className={`${styles["img-price"]} d-flex`}>
                  <img
                    src="/img/bagel-cover.jpg"
                    alt="img"
                    style={{ borderRadius: "30px", width: "240px" }}
                  />
                  <p className={`${styles["price-tag"]} mb-0`}>$ 12.50</p>
                </div>
                <div className="flex-grow-1 d-flex flex-column justify-content-center">
                  <h4>Creamy Bagel</h4>
                  <p className="mb-0">
                    Cream cheese, assortiments de fruits au choix
                  </p>
                  <p className="mb-0">x1</p>
                </div>
              </article>
            </section>
            <section style={{ margin: "5.5rem 0px 2rem 0px" }}>
              <div className="d-flex">
                <h4 className="fw-bolder flex-grow-1">
                  {t("orderRecap:prices.subTotal")}
                </h4>
                <h4 className="fw-bolder">$ 12.50</h4>
              </div>
              <div className="d-flex">
                <h4 className="fw-bolder flex-grow-1">
                  {t("orderRecap:prices.delivery")}
                </h4>
                <h4 className="fw-bolder">$ 12.50</h4>
              </div>
              <div className="d-flex">
                <h4 className="fw-bolder flex-grow-1">
                  {t("orderRecap:prices.service")}
                </h4>
                <h4 className="fw-bolder">$ 12.50</h4>
              </div>
              <div className="d-flex">
                <h4 className="fw-bolder flex-grow-1">
                  {t("orderRecap:prices.donation")}
                </h4>
                <h4 className="fw-bolder">$ 12.50</h4>
              </div>
            </section>
            <section className="my-5">
              <h4>{t("orderRecap:promo.enter")}</h4>
              <h4 style={{ fontWeight: "400" }}>
                {t("orderRecap:promo.enter")}
              </h4>
            </section>
            <section className="d-flex align-items-center my-5">
              <h5 className="flex-grow-1 mb-0">
                <span className="fw-bolder">
                  {t("orderRecap:checkbox.covered")}
                </span>
                ({t("orderRecap:checkbox.cutlury")})
              </h5>
              <Checkbox />
            </section>
            <section className="my-5">
              <div className="d-flex">
                <h4 className="fw-bolder flex-grow-1">
                  {t("orderRecap:prices.total")}
                </h4>
                <h4 className="fw-bolder">$ 12.50</h4>
              </div>
            </section>

            <section
              id={styles["bill"]}
              className="d-flex align-items-center justify-content-center my-5"
            >
              <Paper
                elevation={6}
                sx={{
                  borderRadius: "20px",
                  overflow: "hidden",
                  minWidth: "300px",
                  maxWidth: "550px",
                }}
              >
                <div className={styles["header"]}>
                  <div className="d-flex align-items-center gap-3 mb-3">
                    <img
                      src="/img/landing-page/logo-icon.png"
                      alt="icon"
                      width={60}
                      height={60}
                    />
                    <div>
                      <h5 className="fw-bolder mb-0">
                        {t("orderRecap:billHeader.thankYou")}
                      </h5>
                      <h5 className="fw-bolder mb-0">Romeo</h5>
                    </div>
                  </div>
                  <div className="d-flex align-items-center">
                    <h4 className="mb-0 flex-grow-1">
                      {t("orderRecap:billHeader.orderNum", { num: "1234" })}
                    </h4>
                    <small>Dimanche 10 mai 2021</small>
                  </div>
                </div>
                <div className={styles["body"]}>
                  <div className="row">
                    <div className="col-sm-6  my-3">
                      <h5 className="mb-0 fw-bolder">
                        {t("orderRecap:billBody.orderedOn")}
                      </h5>
                      <h5 className="fw-bolder text-muted mb-0">
                        10 mai 2021 13:30
                      </h5>
                    </div>
                    <div className="col-sm-6 my-3">
                      <h5 className="mb-0 fw-bolder">
                        {t("orderRecap:billBody.deliveredOn")}
                      </h5>
                      <h5 className="fw-bolder text-muted mb-0">
                        10 mai 2021 13:30
                      </h5>
                    </div>
                    <div className="col-sm-6  my-3">
                      <h5 className="mb-0 fw-bolder">
                        {t("orderRecap:billBody.by")}
                      </h5>
                      <h5 className="fw-bolder mb-0">Bagel corner</h5>
                      <h5 className="fw-bolder text-muted mb-0">
                        123, Street 3<br /> New Delhi
                        <br /> India
                      </h5>
                    </div>
                    <div className="col-sm-6  my-3">
                      <h5 className="mb-0 fw-bolder">
                        {t("orderRecap:billBody.to")}
                      </h5>
                      <h5 className="fw-bolder text-muted mb-0">
                        Romeo Bernard
                      </h5>
                      <h5 className="fw-bolder text-muted mb-0">
                        123, Street 3<br /> New Delhi
                        <br /> India
                      </h5>
                    </div>
                  </div>
                  <div className="mb-4">
                    <hr />
                  </div>
                  <h4>
                    {t("orderRecap:billHeader.orderNum", { num: "1234" })}
                  </h4>
                  <div className="d-flex text-muted">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      Bagel Vegetarian
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex text-muted">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      Bagel Vegetarian
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex text-muted">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      Bagel Vegetarian
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex text-muted">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      Bagel Vegetarian
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex mt-4">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      {t("orderRecap:prices.subTotal")}
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex text-muted">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      {t("orderRecap:prices.delivery")}
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex text-muted">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      {t("orderRecap:prices.service")}
                    </h5>
                    <h5 className="fw-bolder mb-1">$ 12.50</h5>
                  </div>
                  <div className="d-flex text-muted mt-4">
                    <h5 className="fw-bolder flex-grow-1 mb-1">
                      {t("orderRecap:prices.credit")}
                    </h5>
                    <h5 className="fw-bolder mb-1">$ -2.00</h5>
                  </div>

                  <div className="d-flex mt-4">
                    <h4 className="fw-bolder flex-grow-1 mb-1">
                      {t("orderRecap:prices.total")}
                    </h4>
                    <h4 className="fw-bolder mb-1">$ 20.00</h4>
                  </div>

                  <div className="mt-4">
                    <DownloadIcon sx={{ fontSize: "48px", color: "gray" }} />
                  </div>
                </div>
              </Paper>
            </section>
          </div>
          <div className="d-none d-md-block col-md-2"></div>
        </div>
      </main>
    </>
  );
};

export default OrderRacap;
