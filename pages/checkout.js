import React from "react";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

import AppNav from "../components/shared/AppNav";
import AuthMenu from "../components/shared/AuthMenu";

import Checkbox from "@mui/material/Checkbox";

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import InfoOutlined from "@mui/icons-material/InfoOutlined";

import styles from "../styles/CheckoutPage.module.scss";

const NavHeader = ({ t }) => {
  return <h4 className="text-white">{t("checkout:appHeader.title")}</h4>;
};

const CheckoutPage = () => {
  const { t } = useTranslation("checkout");
  return (
    <>
      <AppNav header={<NavHeader t={t} />} menu={<AuthMenu />} />
      <main className="container" style={{ paddingTop: "250px" }}>
        <div className="row">
          <div className="d-none d-md-block col-md-2"></div>
          <div className="col-md-8">
            <section id={styles["order-items"]} className="my-5">
              {[...Array(4)].map((i) => {
                   return(
                    <article className={`${styles["item"]} d-flex gap-3`} key={i}>
                    <div className={`${styles["img-price"]} d-flex`}>
                      <img
                        src="/img/bagel-cover.jpg"
                        alt="img"
                        style={{ borderRadius: "30px", width: "240px" }}
                      />
                      <p className={`${styles["price-tag"]} mb-0`}>$ 12.50</p>
                    </div>
                    <div className="flex-grow-1 d-flex flex-column justify-content-center">
                      <h4>Creamy Bagel</h4>
                      <p className="mb-0">
                        Cream cheese, assortiments de fruits au choix
                      </p>
                      <div className="d-flex gap-1 mt-3">
                        <AddCircleOutlineIcon sx={{ color: "#700cbe" }} />
                        <h5 className="fw-bolder m-0">2</h5>
                        <RemoveCircleOutlineIcon sx={{ color: "#700cbe" }} />
                      </div>
                    </div>
                  </article>
                   );
              })}
             
            
            </section>
            <section style={{ margin: "5.5rem 0px 2rem 0px" }}>
              {[...Array(4)].map((i) => {
                           return(
                            <div className="d-flex" key={i}>
                            <h4 className="fw-bolder flex-grow-1">
                              {t("checkout:prices.subTotal")}
                            </h4>
                            <h4 className="fw-bolder">$ 12.50</h4>
                          </div>
                           );
                           } ) }
              
          
            </section>
            <section className="my-4">
              <button
                className="d-flex align-items-center"
                style={{
                  border: "none",
                  background: "none",
                  fontSize: "1.2rem",
                  fontFamily: "Nunito",
                }}
              >
                <AddCircleOutlineIcon
                  sx={{ color: "#700cbe", marginRight: "0.5rem" }}
                />
                <p className="fw-bolder mb-0">
                  {t("checkout:reduction.addReduction")}
                </p>
              </button>
            </section>
            <section className="my-5">
              <h4>{t("checkout:promo.enter")}</h4>
              <form className="d-flex align-items-center gap-2">
                <input
                  type="text"
                  style={{
                    padding: "8px 5px",
                    fontSize: "0.8rem",
                    fontFamily: "Nunito",
                    borderRadius: "25px",
                    border: "2px solid lightgray",
                  }}
                />
                <button className="btn-primary">
                  {t("checkout:promo.add")}
                </button>
              </form>
            </section>
            <section className="d-flex align-items-center my-5">
              <h5 className="flex-grow-1 mb-0">
                <span className="fw-bolder">
                  {t("checkout:checkbox.covered")}
                </span>
                ({t("checkout:checkbox.cutlury")})
              </h5>
              <Checkbox />
            </section>
            <section className="my-5">
              <div className="d-flex">
                <h4 className="fw-bolder flex-grow-1">
                  {t("checkout:prices.total")}
                </h4>
                <h4 className="fw-bolder">$ 12.50</h4>
              </div>
              <Link href="/payment" passHref>
                <button
                  className="btn-primary mt-4"
                  style={{ marginBottom: "150px", fontSize: "1.2rem" }}
                >
                  {t("checkout:goToPayment")}
                </button>
              </Link>
            </section>
          </div>
          <div className="d-none d-md-block col-md-2"></div>
        </div>
      </main>
    </>
  );
};

export default CheckoutPage;
