import { useState } from "react";
import Carousel from "react-elastic-carousel";
import FilterSidebar from "../components/shared/FilterSidebar";
import Layout from "../components/shared/Layout";

import LocalDiningIcon from "@mui/icons-material/LocalDining";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import DeliveryDiningOutlinedIcon from "@mui/icons-material/DeliveryDiningOutlined";
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import QueryBuilderOutlinedIcon from "@mui/icons-material/QueryBuilderOutlined";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";

import styles from "./../styles/Search.module.scss";

const Search = () => {
  const [categoryCarousel] = useState([
    { width: 0, itemsToShow: 2, itemsToScroll: 1 },
    { width: 255, itemsToShow: 3, itemsToScroll: 1 },
    { width: 567, itemsToShow: 5, itemsToScroll: 1 },
    { width: 768, itemsToShow: 7, itemsToScroll: 1 },
    { width: 992, itemsToShow: 9, itemsToScroll: 1 },
  ]);
  return (
    <>
      <Layout
        nav={{
          alertText: "Recommend Giveats to a Friend and win 300 Geeds",
          isAlertVisible: true,
        }}
      >
        <main className="pt-5">
          <section
            className="d-flex align-items-center w-100"
            style={{
              height: "50vh",
              backgroundImage: `url(/img/landing-page/hero-1.png)`,
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              backgroundPosition: "bottom right",
            }}
          >
            <div className="container">
              <div className={`mx-md-5`} style={{ marginTop: "-4.5rem" }}>
                <form onSubmit={(e) => e.preventDefault()}>
                  <div className={`d-md-flex align-items-center gap-3`}>
                    <div className="field-inline-text d-flex align-items-center py-2 px-3 my-3 my-md-0">
                      <span>ic</span>
                      <input type="text" placeholder="Select your location" />
                    </div>
                    <div className="field-inline-text d-flex align-items-center ps-3 pe-1 py-1 my-3 my-md-0">
                      <span>ic</span>
                      <input type="text" placeholder="Select your location" />
                      <button
                        className="btn-primary rounded-pill py-2 px-3"
                        type="submit"
                      >
                        ic
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </section>
          <section id={styles["search-page"]} className="container-fluid">
            <div className="row">
              <div
                id={styles["filter-container"]}
                className="d-none d-md-block col-lg-2 col-md-3"
              >
                <FilterSidebar />
              </div>
              <div className="col-lg-10 col-md-9">
                <div className="container-fluid py-5">
                  <div className="d-flex flex-column flex-md-row align-items-center gap-3">
                    <div className="d-flex gap-2 flex-grow-1">
                      <button
                        className="btn-secondary select"
                        aria-selected={false}
                      >
                        <LocalDiningIcon /> On the Spot
                      </button>
                      <button
                        className="btn-secondary select"
                        aria-selected={false}
                      >
                        <ShoppingBagOutlinedIcon /> Takeaway
                      </button>
                      <button
                        className="btn-secondary select"
                        aria-selected={false}
                      >
                        <DeliveryDiningOutlinedIcon /> Delivery
                      </button>
                    </div>
                    <div className="d-flex gap-2">
                      <button
                        className="btn-secondary select"
                        aria-selected={true}
                      >
                        <CalendarTodayOutlinedIcon /> Today
                      </button>
                      <button
                        className="btn-secondary select"
                        aria-selected={true}
                      >
                        <QueryBuilderOutlinedIcon /> Hour
                      </button>
                      <button
                        className="btn-secondary select"
                        aria-selected={true}
                      >
                        <PersonOutlineOutlinedIcon /> People
                      </button>
                    </div>
                  </div>
                  <div className={`${styles["filter-category"]} py-4`}>
                    <Carousel pagination={false} breakPoints={categoryCarousel}>
                      {[ ...Array(10)].map((i) => {
                           return(
                            <div className="m-sm-3" aria-selected={false} key={i}>
                            <img
                              src="/img/pizza.png"
                              alt="food-category"
                              className="w-100 p-3"
                            />
                          </div>
                           );

                      })}
                    </Carousel>
                  </div>
                  <hr />
                  <h3
                    className="my-4"
                    style={{ fontWeight: 900, color: "#333" }}
                  >
                    Indian food takeaway Restaurants
                  </h3>
                  <div className="row">
                    {[ ...Array(12)].map((i) => {
                            return(
                              <div className="col-lg-3 col-md-4 col-sm-6" key={i}>
                              <div className="m-3">
                                <img
                                  className="w-100"
                                  src="/img/landing-page/arr1.png"
                                />
                                <div className="m-2">
                                  <h5 className="fw-bolder m-0">Crèperie Hermine</h5>
                                  <p className="text-muted">
                                    Français I 4,7 I Livré dans 25min
                                  </p>
                                </div>
                              </div>
                            </div>
                            );
                    })}
                   
                  
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </Layout>
    </>
  );
};

export default Search;
