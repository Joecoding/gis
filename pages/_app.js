import { useRouter } from "next/router";
import { Provider } from "react-redux";
import { useStore } from "../store";

import "./../styles/bootstrap-grid.css";
import "./../styles/bootstrap-utilities.css";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const { locale } = router;

  const store = useStore({ language: { locale } });

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
