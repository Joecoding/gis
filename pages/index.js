import { useState } from "react";
import Head from "next/head";
import useTranslation from "next-translate/useTranslation";
// import Image from "next/image";
import Link from "next/link";
import Navbar from "../components/shared/Navbar";
import Hero from "../components/LandingPage/Hero";
import CarouselCard from "../components/LandingPage/CarouselCard";
import Carousel from "react-elastic-carousel";
import styles from "../styles/Home.module.scss";
import Layout from "../components/shared/Layout";
import NewsLetter from "../components/LandingPage/NewsLetter";

const Home = () => {
    const { t } = useTranslation("home");
  
    const [categoryCarousel] = useState([
        { width: 0, itemsToShow: 2, itemsToScroll: 1 },
        { width: 255, itemsToShow: 3, itemsToScroll: 1 },
        { width: 567, itemsToShow: 5, itemsToScroll: 1 },
        { width: 768, itemsToShow: 7, itemsToScroll: 1 },
        { width: 992, itemsToShow: 9, itemsToScroll: 1 },
    ]);

    return (
        <>
            <Head>
                <title>Giveats</title>
                <meta name="description" content="Giveats - enjoy eatings" />
            </Head>

            <Layout>
                <main className="pt-5">

                    <Hero t={t} />
                    <section
                        id={styles.features}
                        className="container py-3 my-5"
                    >
                        <div className="row">
                            <div className="col-md-4">
                                <div className="d-flex justify-content-center">
                                    <div
                                        className={`${styles.circle} ${styles.orange} d-flex flex-column mb-3`}
                                    >
                                        <img
                                            src="/img/landing-page/qr-code.png"
                                            alt="feacture-background"
                                            width={80}
                                        />
                                    </div>
                                </div>
                                <div
                                    className={`${styles["feature-text"]} text-center px-3`}
                                >
                                    <h5 className="fw-bolder">
                                        {t("home:featureCircle.qrCode")}
                                    </h5>
                                    <small>
                                        {t("home:featureCircle.qrCodeText")}
                                    </small>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="d-flex justify-content-center">
                                    <div
                                        className={`${styles.circle} ${styles.purple} d-flex flex-column mb-3`}
                                    >
                                        <img
                                            src="/img/landing-page/smartphone.png"
                                            alt="feacture-background"
                                            width={80}
                                        />
                                    </div>
                                </div>
                                <div
                                    className={`${styles["feature-text"]} text-center px-3`}
                                >
                                    <h5 className="fw-bolder">
                                        {t("home:featureCircle.prePayment")}
                                    </h5>
                                    <small>
                                        {t("home:featureCircle.prePaymentText")}
                                    </small>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="d-flex justify-content-center">
                                    <div
                                        className={`${styles.circle} ${styles.orange} d-flex flex-column mb-3`}
                                    >
                                        <img
                                            src="/img/landing-page/delivery-man.png"
                                            alt="feacture-background"
                                            width={80}
                                        />
                                    </div>
                                </div>
                                <div
                                    className={`${styles["feature-text"]} text-center px-3`}
                                >
                                    <h5 className="fw-bolder">
                                        {t("home:featureCircle.delivery")}
                                    </h5>
                                    <small>
                                        {t("home:featureCircle.deliveryText")}
                                    </small>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id={styles["download-app"]}>
                        <div className="container pt-5 pb-md-7 pb-8">
                            <div className="text-white d-flex flex-column flex-md-row align-items-center gap-3 mb-4">
                                <div className="d-flex align-items-center flex-grow-1">
                                    <img
                                        src="/img/landing-page/logo-icon.png"
                                        alt="logo icon"
                                        width={60}
                                        height={60}
                                    />
                                    <h2 className="m-0 ms-3">
                                        {t("home:moreApp")}
                                    </h2>
                                </div>
                                <div className="d-flex align-items-center gap-1">
                                    <button className="app-btn">
                                        <img
                                            src="/img/landing-page/playstore.png"
                                            alt="Download Android App"
                                            width={130}
                                        />
                                    </button>
                                    <button className="app-btn">
                                        <img
                                            src="/img/landing-page/appstore.png"
                                            alt="Download IOS App"
                                            width={130}
                                        />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="container">
                        <div
                            className={`${styles["filter-category"]} pt-4 pb-5`}
                        >
                            <Carousel
                                pagination={false}
                                breakPoints={categoryCarousel}
                            >
                                {[...Array(15)].map((i) => {
                                    return (
                                        <div key={i} className="m-sm-3">
                                            <img
                                                src="/img/pizza.png"
                                                alt="food-category"
                                                className="w-100 p-3"
                                            />
                                        </div>
                                    );
                                })}
                            </Carousel>
                        </div>
                    </section>
                
          
                 {/* CarouselCard */}
                   <CarouselCard t={t} />

                  
                  {/* NewsLetter */}
                    <NewsLetter t={t} />
                    
                </main>
            </Layout>
        </>
    );
};

export default Home;
