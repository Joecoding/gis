import { TextField, Checkbox } from "@mui/material";
import Layout from "../components/shared/Layout";
import Link from "next/link";

import styles from "./../styles/Signup.module.scss";

const signup = () => {
  return (
    <>
      <Layout>
        <main id={styles["sign"]} className="py-8">
          <div className="container ">
            <div className="row">
              <div className="d-none d-md-block col-md-3 col-lg-4"></div>
              <div className="col-md-6 col-lg-4">
                <h5 className="mt-5 mb-3">SIGN UP</h5>
                <div className="d-flex flex-column align-items-center gap-3">
                  <button
                    className={`${styles["sign-btn"]} ${styles["facebook"]}`}
                  >
                    <span>
                      <img
                        src="/img/icons/facebook.png"
                        alt="facebook"
                        className="w-100"
                      />
                    </span>
                    Continue with Facebook
                  </button>
                  <button
                    className={`${styles["sign-btn"]} ${styles["google"]}`}
                  >
                    <span>
                      <img
                        src="/img/icons/google.png"
                        alt="facebook"
                        className="w-100"
                      />
                    </span>
                    Continue with Google
                  </button>
                  <div className={styles["strike"]}>
                    <span>or</span>
                  </div>

                  <TextField
                    label="Full Name"
                    variant="outlined"
                    sx={{ width: "100%" }}
                  />
                  <TextField
                    label="Your email id"
                    variant="outlined"
                    sx={{ width: "100%" }}
                  />
                  <TextField
                    label="Your password"
                    variant="outlined"
                    sx={{ width: "100%" }}
                  />
                  <div className="d-flex">
                    <Checkbox size="small" />
                    <p className="mt-3 m-0">
                      I agree Giveats <Link href="/#">Terms of Service</Link>,
                      <Link href="/#">Privacy Policy</Link>,
                      <Link href="/#">Content Policies</Link>
                    </p>
                  </div>

                  <button
                    className={`${styles["sign-btn"]}`}
                    aria-disabled="true"
                  >
                    Sign Up
                  </button>
                  <div className={`${styles["sign-footer"]} text-gray`}>
                    <p className="text-center mb-2">
                      Already have an account? <Link href="/login">Login</Link>
                    </p>
                    <small className="text-center">
                      By continuing, you agree to our T&Cs and Privacy Policy,
                      for more details click <Link href="/#">here</Link>
                    </small>
                  </div>
                </div>
              </div>
              <div className="d-none d-md-block col-md-3 col-lg-4"></div>
            </div>
          </div>
        </main>
      </Layout>
    </>
  );
};

export default signup;
