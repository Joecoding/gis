import React from "react";
import Button from "@mui/material/Button";

const PrimaryButton = ({ children, sx }) => {
  return (
    <>
      <Button
        sx={{
          backgroundColor: "#700cbe",
          borderRadius: "25px",
          color: "white",
          paddding: "1.2rem 0.5rem",
          ...sx,
        }}
      >
        {children}
      </Button>
    </>
  );
};

export default PrimaryButton;
