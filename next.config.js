const nextTranslate = require("next-translate");

module.exports = {
  reactStrictMode: true,
  ...nextTranslate(),
  images: {
    domains: ["cdn-icons-png.flaticon.com"],
  },
};
