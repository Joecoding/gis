import { useState } from "react";

import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

import DialogWrapper from "../shared/DialogWrapper";

const FoodItemDialog = ({ children, title }) => {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        handler={children}
        title={title}
      >
        <div className="p-4 mx-5">
          <div className="text-center">
            <img
              src="/img/bagel-cover.jpg"
              alt="food-item"
              style={{ borderRadius: "20px", width: "100%" }}
            />
          </div>
          <div>
            <small className="text-muted">Description of the dish</small>
            <div>icons</div>
          </div>
          <div>
            <small className="text-muted">List of allergens</small>
            <div className="d-flex gap-2 align-items-center flex-wrap py-2">
              <div className="btn-secondary px-2 py-1">Gluten</div>
              <div className="btn-secondary px-2 py-1">Lactose</div>
              <div className="btn-secondary px-2 py-1">Arachide</div>
              <div className="btn-secondary px-2 py-1">Soja</div>
            </div>
          </div>
          <div className="variants">
            <h5 className="fw-bolder my-2">Size</h5>
            <div className="d-flex flex-column px-2">
              <FormControlLabel
                sx={{ margin: "-0.5rem" }}
                control={<Checkbox size="small" />}
                label="Small"
              />
              <FormControlLabel
                sx={{ margin: "-0.5rem" }}
                control={<Checkbox size="small" />}
                label="Medium"
              />
              <FormControlLabel
                sx={{ margin: "-0.5rem" }}
                control={<Checkbox size="small" />}
                label="Large"
              />
            </div>
          </div>
        </div>
      </DialogWrapper>
    </>
  );
};

export default FoodItemDialog;
