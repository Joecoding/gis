import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import AuthMenu from "../shared/AuthMenu";

import AppNav from "../shared/AppNav";
import CartButtons from "../shared/buttons/CartButtons";

import StarOutlineIcon from "@mui/icons-material/StarOutline";

import styles from "../../styles/RestaurantPage/RestaurantPageParent.module.scss";

const NavHeader = () => {
  return <h4 className="text-white">Restaurant Name</h4>;
};

const RestaurantPageParent = ({ children }) => {
  const router = useRouter();

  const { restaurantId } = router.query;

  return (
    <>
      <AppNav header={<NavHeader />} menu={<AuthMenu />}></AppNav>
      <div
        style={{
          backgroundImage: "url(/img/bagel-cover.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "450px",
          backgroundPosition: "center center",
        }}
      ></div>
      <div className="secondary-nav" style={{ marginTop: "-50px" }}>
        <div
          style={{
            borderRadius: "20px 20px 0px 0px",
            backgroundColor: "#700cbe",
            paddingBottom: "20px",
            // height: "80px",
          }}
        >
          <div className="container">
            <div className="d-flex align-items-center py-4 text-white ">
              <h4 className="flex-grow-1 m-0 ">Havaien Food | $$</h4>
              <StarOutlineIcon />
            </div>
          </div>
        </div>
        <div
          style={{
            borderRadius: "20px 20px 0px 0px",
            backgroundColor: "white",
            marginTop: "-20px",
          }}
        >
          <div className="container">
            <div id={styles["browse-restaurant"]} className="position-relative">
              <div className="d-flex align-items-center justify-content-center gap-2 py-3 mx-2 ">
                <Link href={`/restaurants/${restaurantId}/menu`}>
                  <div
                    className={`${styles["tab"]} ${
                      router.pathname.includes("menu") ? styles["active"] : ""
                    }`}
                  >
                    Menu
                  </div>
                </Link>
                <Link href={`/restaurants/${restaurantId}/discount`}>
                  <div
                    className={`${styles["tab"]} ${
                      router.pathname.includes("discount")
                        ? styles["active"]
                        : ""
                    }`}
                  >
                    Discounts
                  </div>
                </Link>
                <Link href={`/restaurants/${restaurantId}/info`}>
                  <div
                    className={`${styles["tab"]} ${
                      router.pathname.includes("info") ? styles["active"] : ""
                    }`}
                  >
                    Info
                  </div>
                </Link>
                <Link href={`/restaurants/${restaurantId}/opinion`}>
                  <div
                    className={`${styles["tab"]} ${
                      router.pathname.includes("opinion")
                        ? styles["active"]
                        : ""
                    }`}
                  >
                    Opinion
                  </div>
                </Link>
              </div>
              <div
                style={{
                  position: "absolute",
                  right: "0",
                  top: "50%",
                  transform: "translateY(-50%)",
                }}
              >
                <CartButtons />
              </div>
            </div>
          </div>
          {children}
        </div>
      </div>
    </>
  );
};

export default RestaurantPageParent;
