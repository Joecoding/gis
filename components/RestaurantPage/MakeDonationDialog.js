import React from "react";
import Link from "next/link";

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";

import DialogWrapper from "../shared/DialogWrapper";

const MakeDonationDialog = ({
  open,
  handleClickOpen,
  handleClose,
  children,
}) => {
  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        handler={children}
        sx={{
          "& .MuiPaper-root": {
            padding: "1rem 2rem",
            borderRadius: "20px",
            minWidth: "325px",
            backgroundColor: "#700cb9",
            color: "white",
          },
        }}
        title={"Micro-don alimentaire"}
      >
        <div className="py-3 px-4">
          <p className="fw-bolder">
            Souhaitez-vous laisser un micro-don pour participer a financer un
            repas a une personne en situation de precarite ?
          </p>
          <p>
            Giveats s'engage et propose un moyen simple et efficace de prendre
            part au combat quotidien contre l'insecruite alimentaire pour des
            milliers de personnes.
          </p>
          <div className="d-flex justify-content-end">
            <Link href="/donations" passHref>
              <a>En savoir plus</a>
            </Link>
          </div>
          <div className="d-flex justify-content-center gap-2 my-4">
            <AddCircleOutlineIcon />
            <h4 className="m-0">$ 2</h4>
            <RemoveCircleOutlineIcon />
          </div>
          <div className="d-flex align-items-center justify-content-center gap-3">
            <button className="btn-primary">Non, merci</button>
            <button className="btn-secondary">Laissez un pourboire</button>
          </div>
        </div>
      </DialogWrapper>
    </>
  );
};

export default MakeDonationDialog;
