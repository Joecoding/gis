import React, { Fragment, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { setLang } from "../../actions/language";

import MenuWrapper from "./MenuWrapper";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import CheckIcon from "@mui/icons-material/Check";

const LangSelect = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [languages] = useState([
    {
      locale: "en",
      flagUrl: "https://cdn-icons-png.flaticon.com/512/299/299722.png",
      alt: "English",
    },
    {
      locale: "fr",
      flagUrl: "https://cdn-icons-png.flaticon.com/512/299/299753.png",
      alt: "French",
    },
    {
      locale: "es",
      flagUrl: "https://cdn-icons-png.flaticon.com/512/299/299820.png",
      alt: "Spanish",
    },
  ]);
  const locale = useSelector((state) => state.language.locale);

  const [currentLocale, setCurrentLocale] = useState(
    languages.find((lang) => lang.locale === locale)
  );

  useEffect(() => {
    setCurrentLocale(languages.find((lang) => lang.locale === locale));
  }, [locale]);

  const changeLocale = (val) => {
    dispatch(setLang(val.locale));
    router.push(router.pathname, router.pathname, { locale: val.locale });
    setIsFocused(false);
  };

  const [anchorEl, setAnchorEl] = useState(null);
  const [isFocused, setIsFocused] = useState(false);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    setIsFocused(true);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setIsFocused(false);
  };

  return (
    <>
      <div
        className="d-flex align-items-center gap-1"
        style={{
          boxShadow: "2px 2px 6px -3px black",
          cursor: "pointer",
          background: "white",
          borderRadius: "10px",
          padding: "5px 10px",
        }}
        onClick={handleClick}
      >
        <img
          src={currentLocale.flagUrl}
          alt={currentLocale.alt}
          width={25}
          height={25}
        ></img>
        <KeyboardArrowDownIcon />
      </div>
      <MenuWrapper
        anchorEl={anchorEl}
        open={isFocused}
        handleClose={handleClose}
        anchor={{ vertical: "top", horizontal: "right" }}
        transform={{ vertical: "top", horizontal: "right" }}
      >
        {languages.map((lang, i) => {
          return (
            <li key={i} onClick={() => changeLocale(lang)}>
              <button
                className="d-flex align-items-center  gap-1"
                style={{
                  backgroundColor: "white",
                  border: "none",
                  padding: "5px 10px",
                }}
              >
                <img
                  src={lang.flagUrl}
                  alt={lang.alt}
                  width={25}
                  height={25}
                ></img>
                {lang.locale === locale ? <CheckIcon /> : ""}
              </button>
            </li>
          );
        })}
      </MenuWrapper>
    </>
  );
};

export default LangSelect;
