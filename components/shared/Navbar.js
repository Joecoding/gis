import { useState } from "react";
import Link from "next/link";
import Image from "next/image";

import useTraslation from "next-translate/useTranslation";

import NavAlert from "./NavAlert";
import LangSelect from "./LangSelect";
import SidebarMenuButton from "./buttons/SidebarMenuButton";
import AuthMenu from "./AuthMenu";

import styles from "./../../styles/shared/Navbar.module.scss";

const Navbar = ({ isAlertVisible, alertText }) => {
  const { t } = useTraslation("common");

  const [isAlert, setIsAlert] = useState(isAlertVisible);
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleDrawer = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  return (
    <>
      <nav id={styles.navbar}>
        <div className={`${styles["nav-holder"]} d-flex align-items-center`}>
          <div className="container">
            <div className={styles.navbar}>
              <div className="d-flex align-items-center">
                <Link href="/">
                  <Image
                    src="/img/landing-page/logo.png"
                    alt="Giveats"
                    width={170}
                    height={42}
                    className="pointer"
                  />
                </Link>
              </div>
              <div className="d-none d-lg-flex gap-3 align-items-center">
                <Link href="/#">{t("common:nav.restaurantSetup")}</Link>
                <Link href="/#">{t("common:nav.associationSetup")}</Link>
                <Link href="/login">
                  <button className="btn-primary btn-shadow">
                    {t("common:nav.signIn")}
                  </button>
                </Link>
                <Link href="/signup">
                  <button className="btn-secondary btn-shadow">
                    {t("common:nav.signUp")}
                  </button>
                </Link>
              </div>
              <div className="d-flex gap-3 align-items-center">
                <div>
                  <LangSelect />
                </div>
                <SidebarMenuButton>
                  <AuthMenu />
                </SidebarMenuButton>
              </div>
            </div>
          </div>
        </div>
        {isAlert && (
          <div>
            <NavAlert alertText={alertText} setIsAlert={setIsAlert} />
          </div>
        )}
      </nav>
    </>
  );
};

export default Navbar;
