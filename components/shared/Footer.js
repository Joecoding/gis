import React from "react";
import Link from "next/link";

import useTranslation from "next-translate/useTranslation";

import styles from "./../../styles/shared/Footer.module.scss";

const Footer = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <footer id={styles.footer} className="text-white pt-6">
        <div className="container mb-6">
          <div className="row mt-4">
            <div className="col-md-9">
              <div className="row">
                <div className="col-lg-3 col-md-6 mb-4">
                  <h5 className="fw-bolder mb-4">
                    {t("common:footer.aboutGiveats")}
                  </h5>
                  <div className="tex-start d-flex flex-column gap-2">
                    <Link href="/#">{t("common:footer.whoAreWe")}</Link>
                    <Link href="/#">{t("common:footer.aboutUs")}</Link>
                    <Link href="/#">{t("common:footer.contactUs")}</Link>
                    <Link href="/#">{t("common:footer.newsroom")}</Link>
                    <Link href="/#">{t("common:footer.blog")}</Link>
                    <Link href="/#">{t("common:footer.careers")}</Link>
                    <Link href="/#">{t("common:footer.ourImpact")}</Link>
                    <Link href="/#">{t("common:footer.becomePartner")}</Link>
                  </div>
                </div>
                <div className="col-lg-3  col-md-6 mb-4">
                  <h5 className="fw-bolder mb-4">
                    {t("common:footer.forFoodies")}
                  </h5>
                  <div className="tex-start d-flex flex-column gap-2">
                    <Link href="/#">{t("common:footer.nearby")}</Link>
                    <Link href="/#">{t("common:footer.promoFirst")}</Link>
                    <Link href="/#">{t("common:footer.loyaltyProg")}</Link>
                    <Link href="/#">{t("common:footer.userFaq")}</Link>
                    <Link href="/#">{t("common:footer.download")}</Link>
                    <Link href="/#">{t("common:footer.taker")}</Link>
                  </div>
                </div>
                <div className="col-lg-3  col-md-6 mb-4">
                  <h5 className="fw-bolder mb-4">
                    {t("common:footer.forRestaurant")}
                  </h5>
                  <div className="tex-start d-flex flex-column gap-2">
                    <Link href="/#">{t("common:footer.register")}</Link>
                    <Link href="/#">{t("common:footer.dashboard")}</Link>
                    <Link href="/#">{t("common:footer.demo")}</Link>
                    <Link href="/#">{t("common:footer.suggest")}</Link>
                    <Link href="/#">{t("common:footer.resources")}</Link>
                    <Link href="/#">{t("common:footer.restaurantFaq")}</Link>
                  </div>
                </div>
                <div className="col-lg-3  col-md-6 mb-4">
                  <h5 className="fw-bolder mb-4">
                    {t("common:footer.forYou")}
                  </h5>
                  <div className="tex-start d-flex flex-column gap-2">
                    <Link href="/#">{t("common:footer.terms")}</Link>
                    <Link href="/#">{t("common:footer.privacy")}</Link>
                    <Link href="/#">{t("common:footer.cookiesPolicy")}</Link>
                    <Link href="/#">{t("common:footer.reportFraud")}</Link>
                    <Link href="/#">{t("common:footer.legalNotice")}</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <h5 className="fw-bolder mb-4">{t("common:footer.atHands")}</h5>
              <div className="d-flex align-items-center gap-1 mb-4">
                <button className="app-btn">
                  <img
                    src="/img/landing-page/playstore.png"
                    alt="Download Android App"
                    width={130}
                  />
                </button>
                <button className="app-btn">
                  <img
                    src="/img/landing-page/appstore.png"
                    alt="Download IOS App"
                    width={130}
                  />
                </button>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="container py-4">
          <div className="d-flex align-items-center">
            <small className="flex-grow-1">
              {t("common:footer.copyright")}
            </small>
            <div className={`${styles.social} d-flex gap-3`}>
              <Link href="/#">
                <img
                  src="/img/landing-page/facebook.png"
                  alt="facebook"
                  width={23}
                />
              </Link>
              <Link href="/#">
                <img
                  src="/img/landing-page/twitter.png"
                  alt="twitter"
                  width={23}
                />
              </Link>
              <Link href="/#">
                <img
                  src="/img/landing-page/instagram.png"
                  alt="instagram"
                  width={23}
                />
              </Link>
              <Link href="/#">
                <img
                  src="/img/landing-page/linkedin.png"
                  alt="linkedin"
                  width={23}
                />
              </Link>
              <Link href="/#">
                <img
                  src="/img/landing-page/youtube.png"
                  alt="youtube"
                  width={23}
                />
              </Link>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
