import React from "react";

import Box from "@mui/material/Box";

const Sidebar = ({ children }) => {
  return (
    <>
      <Box sx={{ width: "300px" }}>{children}</Box>
    </>
  );
};

export default Sidebar;
