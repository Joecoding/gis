import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

const StyledDialog = styled(Dialog)(({ theme, sx }) => ({
  "& .MuiPaper-root": {
    padding: "1rem 2rem",
    borderRadius: "20px",
    minWidth: "325px",
  },
  "& .MuiDialogContent-root": {
    padding: "0px",
    borderTop: "none",
    borderBottom: "none",
  },
  ...sx,
}));

export default function DialogWrapper({
  open,
  handleClickOpen,
  handleClose,
  title,
  children,
  sx,
}) {
  if (open === undefined || !handleClickOpen || !handleClose) {
    return (
      <>
        <div>Mui Dialog is not configured right</div>
      </>
    );
  }
  return (
    <div>
      {/* <Box onClick={handleClickOpen}>{handler}</Box> */}
      {/* <Button variant="outlined" onClick={handleClickOpen}>
        Open dialog
      </Button> */}
      <StyledDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        sx={sx}
        maxWidth={"sm"}
        fullWidth={true}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            m: 0,
            padding: "0.5rem",
            border: "none",
            position: "relative",
            gap: "1rem",
          }}
        >
          {title ? (
            <>
              <h4 className="mb-0 me-5 flex-grow-1">{title}</h4>
            </>
          ) : (
            <>
              <h4 className="mb-0 me-5 flex-grow-1">&nbsp;</h4>
            </>
          )}
          <IconButton
            aria-label="close"
            onClick={handleClose}
            sx={{
              position: "absolute",
              right: 0,
              //   top: 10,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <DialogContent>{children}</DialogContent>
      </StyledDialog>
    </div>
  );
}
