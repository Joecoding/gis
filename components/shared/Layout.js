import React from "react";
import Navbar from "./Navbar";
import Footer from "./Footer";

const Layout = ({ nav, children }) => {
  return (
    <>
      <header>
        <Navbar
          alertText={nav && nav.alertText}
          isAlertVisible={nav && nav.isAlertVisible}
        />
      </header>
      {children}
      <Footer />
    </>
  );
};

export default Layout;
