import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import {useState, useEffect} from "react"
import styles from "./../../styles/shared/FilterSidebar.module.scss";

const FilterSidebar = () => {
   const [category, setCategory] = useState();
   const [regime, setRegime] = useState();

   useEffect(() => {
    

   }, [])

   let categories =[

   { name: 'Pizza' }, 
   { name: 'Pizza' },
   { name: 'Pizza' },
   { name: 'Pizza' }, 
   { name: 'Pizza' }, 
   { name: 'Pizza' }, 
   { name: 'Pizza' }, 
   { name: 'Pizza' }, 
   { name: 'Pizza' }, 
   { name: 'Pizza' }, 
   { name: 'Pizza' },
   { name: 'Pizza' },
   { name: 'Pizza' },
   
  ]


  return (
    <>
      <section id={styles["filter-sidebar"]} className="px-3 mt-3">
        <article className={styles["filter-one"]}>
          <h5 className="fw-bolder my-2">Categories</h5>
          <div className="d-flex flex-column px-2">
            {categories.map((i, index) => {
                 return(
                  <FormControlLabel
                  sx={{ margin: "-0.5rem" }}
                  control={<Checkbox size="small" />}
                  label={i.name}
                  key={i}
                />
                 );
            })}
            
           
           
          </div>
        </article>
        <article>
          <h5 className="fw-bolder my-2">Regime</h5>
          <div className="d-flex flex-column px-2">
            {[...Array(10)].map((i) => {
                  return(
                    <FormControlLabel
                    sx={{ margin: "-0.5rem" }}
                    control={<Checkbox size="small" />}
                    label="Pizza"
                    key={i}
                  />
                  );
            })}
            
           
          </div>
        </article>
      </section>
    </>
  );
};

export default FilterSidebar;
