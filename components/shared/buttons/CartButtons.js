import { useState } from "react";
import Link from "next/link";

import Checkbox from "@mui/material/Checkbox";

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { KeyboardArrowDown } from "@mui/icons-material";
import MenuWrapper from "../MenuWrapper";
import MakeDonationDialog from "../../RestaurantPage/MakeDonationDialog";

const CartButtons = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [isDonationDailogOpen, setIsDonationDialogOpen] = useState(false);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    if (!isDonationDailogOpen) {
      setAnchorEl(null);
    }
  };

  const openDonationDialog = () => {
    setIsDonationDialogOpen(true);
  };
  const closeDonationDialog = () => {
    setIsDonationDialogOpen(false);
  };

  const toRedirectOrShowModal = () => {
    openDonationDialog();
  };

  return (
    <>
      <div
        className="d-flex align-items-center gap-1 px-3 py-2"
        style={
          !open
            ? {
                boxShadow: "1px 1px 9px -2px black",
                cursor: "pointer",
                borderRadius: "20px",
              }
            : { cursor: "pointer", borderRadius: "20px" }
        }
        onClick={handleClick}
      >
        <ShoppingCartIcon sx={{ color: "#700cbe" }} />
        $12.57
        <KeyboardArrowDown fontSize="small" sx={{ color: "gray" }} />
      </div>
      <MenuWrapper
        anchorEl={anchorEl}
        open={open}
        handleClose={handleClose}
        anchor={{ vertical: "top", horizontal: "right" }}
        transform={{ vertical: "top", horizontal: "right" }}
        sx={{
          "& .MuiPaper-root": {
            borderRadius: "20px",
            boxShadow: "1px 1px 9px -2px black",
          },
        }}
      >
        <div
          className="p-3"
          style={{
            width: "20rem",
            position: "relative",
            background: "white",
            minHeight: "100px",
            maxHeight: "95vh",
            overflow: "auto",
          }}
        >
          <div style={{ position: "absolute", top: "0px", right: "0px" }}>
            <div
              className="d-flex align-items-center gap-1 px-3 py-2"
              style={{
                boxShadow: "1px 1px 9px -2px black",
                cursor: "pointer",
                borderRadius: "20px",
              }}
              onClick={handleClose}
            >
              <ShoppingCartIcon sx={{ color: "#700cbe" }} />
              $12.57
              <KeyboardArrowDown fontSize="small" sx={{ color: "gray" }} />
            </div>
          </div>
          <div className="mt-5">
            <div className="d-flex align-items-center gap-3 my-2">
              <img
                src="/img/bagel-cover.jpg"
                alt="img"
                className="rounded-circle"
                width={60}
                height={60}
              />
              <div className="flex-grow-1">
                <div className="d-flex align-items-center gap-2">
                  <p className="mb-0 fw-bolder flex-grow-1">Sweet Bagel</p>
                  <small className="mb-0 fw-bolder text-muted">$ 12.50</small>
                </div>
                <p
                  className="text-muted mb-1"
                  style={{ maxWidth: "150px", fontSize: ".7rem" }}
                >
                  Cream Cheese, assortiments de fruits au choix
                </p>
                <div className="d-flex gap-2">
                  <AddCircleOutlineIcon
                    fontSize="small"
                    sx={{ color: "#700cbe" }}
                  />
                  <p className="m-0">2</p>
                  <RemoveCircleOutlineIcon
                    fontSize="small"
                    sx={{ color: "#700cbe" }}
                  />
                </div>
              </div>
            </div>
            <div className="d-flex align-items-center gap-3 my-2">
              <img
                src="/img/bagel-cover.jpg"
                alt="img"
                className="rounded-circle"
                width={60}
                height={60}
              />
              <div className="flex-grow-1">
                <div className="d-flex align-items-center gap-2">
                  <p className="mb-0 fw-bolder flex-grow-1">Sweet Bagel</p>
                  <small className="mb-0 fw-bolder text-muted">$ 12.50</small>
                </div>
                <p
                  className="text-muted mb-1"
                  style={{ maxWidth: "150px", fontSize: ".7rem" }}
                >
                  Cream Cheese, assortiments de fruits au choix
                </p>
                <div className="d-flex gap-2">
                  <AddCircleOutlineIcon
                    fontSize="small"
                    sx={{ color: "#700cbe" }}
                  />
                  <p className="m-0">2</p>
                  <RemoveCircleOutlineIcon
                    fontSize="small"
                    sx={{ color: "#700cbe" }}
                  />
                </div>
              </div>
            </div>
            <div>
              <div className="d-flex gap-4">
                <p className="fw-bolder mb-0 flex-grow-1">
                  {/* {t("orderRecap:prices.subTotal")} */}
                  Sous-total
                </p>
                <p className="fw-bolder mb-0">$ 12.50</p>
              </div>
              <div className="d-flex gap-4">
                <p className="fw-bolder mb-0 flex-grow-1">
                  {/* {t("orderRecap:prices.delivery")} */}
                  Frais de livraison
                </p>
                <p className="fw-bolder mb-0">$ 12.50</p>
              </div>
              <div className="d-flex gap-4">
                <p className="fw-bolder mb-0 flex-grow-1 d-flex align-items-center">
                  {/* {t("orderRecap:prices.service")} */}
                  Frais de service&nbsp;
                  <InfoOutlinedIcon fontSize="small" />
                </p>
                <p className="fw-bolder mb-0">$ 12.50</p>
              </div>
              <div className="d-flex gap-4 my-4">
                <p className="fw-bolder mb-0 flex-grow-1 d-flex align-items-center">
                  {/* {t("orderRecap:prices.donation")} */}
                  Micro-don alimentaire&nbsp;
                  <InfoOutlinedIcon fontSize="small" />
                </p>
                <div className="d-flex gap-1">
                  <AddCircleOutlineIcon
                    fontSize="small"
                    sx={{ color: "#700cbe" }}
                  />
                  <p className="m-0">2</p>
                  <RemoveCircleOutlineIcon
                    fontSize="small"
                    sx={{ color: "#700cbe" }}
                  />
                </div>
              </div>
            </div>
            <div className="d-flex align-items-center my-5">
              <p className="flex-grow-1 mb-0" style={{ fontSize: ".8rem" }}>
                <span className="fw-bolder">
                  {/* {t("orderRecap:checkbox.covered")} */}
                  Couverts&nbsp;
                </span>
                {/* ({t("orderRecap:checkbox.cutlury")}) */}
                (luttez contre le gaspillage el ne prenez des couverts que si
                necessaire)
              </p>
              <Checkbox />
            </div>

            {/* <Link href="/checkout" passHref> */}
            <MakeDonationDialog
              open={isDonationDailogOpen}
              handleClickOpen={openDonationDialog}
              handleClose={closeDonationDialog}
            >
              <button
                className="btn-primary d-flex justify-content-center w-100"
                onClick={() => toRedirectOrShowModal()}
              >
                <ShoppingCartIcon sx={{ marginRight: "1rem" }} /> Valider le
                panier
              </button>
            </MakeDonationDialog>
            {/* </Link> */}
          </div>
        </div>
      </MenuWrapper>
    </>
  );
};

export default CartButtons;
