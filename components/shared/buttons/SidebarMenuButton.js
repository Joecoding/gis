import React, { useState } from "react";

import Sidebar from "../Sidebar";
// import NavMenu from "../NavMenu";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

const SidebarMenuButton = ({ children }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleDrawer = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };
  return (
    <>
      <div
        style={{
          borderRadius: "10px",
          cursor: "pointer",
          boxShadow: "2px 2px 6px -3px black",
        }}
        onClick={() => toggleDrawer()}
      >
        <div
          className="d-flex align-items-center p-2"
          style={{
            background: "white",
            borderRadius: "10px",
            // border: "1px solid black",
          }}
        >
          <img
            src="/img/icons/menu-burger.png"
            alt="Menu"
            width={20}
            height={20}
          ></img>
          <small className=" fw-bolder m-0 ms-2">MENU</small>
        </div>
      </div>
      <Drawer
        anchor={"right"}
        open={isSidebarOpen}
        onClose={() => toggleDrawer()}
        style={{ width: "300px" }}
        sx={{
          "& .MuiPaper-root": {
            display: "inline",
            height: "100%",
            "& .MuiBox-root": { height: "100%", marginRight: "0px" },
          },
        }}
      >
        <Sidebar>
          <div
            className="mx-3 mb-3 h-100"
            style={{ position: "relative", paddingTop: "3.5rem" }}
          >
            <div style={{ position: "absolute", top: "2%", right: "2%" }}>
              <IconButton aria-label="Close" onClick={() => toggleDrawer()}>
                <CloseIcon />
              </IconButton>
            </div>
            {/* <div className="d-flex justify-content-end mb-2">
            </div> */}
            {children}
          </div>
        </Sidebar>
      </Drawer>
    </>
  );
};

export default SidebarMenuButton;
