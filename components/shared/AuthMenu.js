import React, { useState } from "react";
import Link from "next/link";

import LangSelect from "./LangSelect";

import useTranslation from "next-translate/useTranslation";

import styles from "./../../styles/shared/AuthMenu.module.scss";

const LoggedInMenu = ({ onLogout }) => {
  const { t } = useTranslation("common");
  return (
    <>
      <section className={`${styles["auth-menu-layout"]}`}>
        <header
          className={`${styles["layout-header"]} d-flex align-items-center`}
        >
          <div className="flex-grow-1">
            <img
              src="/img/landing-page/logo.png"
              alt="logo"
              width={170}
              height={42}
            />
          </div>
          <div>
            <img
              src="/img/icons/menu-burger.png"
              alt="menu"
              width={20}
              height={20}
            />
          </div>
        </header>
        <ul className="p-0 my-3 flex-grow-1" style={{ listStyle: "none" }}>
          <Link href="/profile">
            <li className="p-1 m-1 pointer fw-bolder">
              {t("common:loggedOut.myProfile")}
            </li>
          </Link>
          <li className="p-1 m-1 pointer fw-bolder">
            {t("common:loggedOut.orderHistory")}
          </li>
          <li className="p-1 m-1 pointer fw-bolder">
            {t("common:loggedOut.faq")}
          </li>
        </ul>
        <div className={`${styles["bottom-actions"]}`}>
          {/* <div className="d-flex align-items-center"> */}
          <div className="flex-grow-1">
            <button
              style={{
                backgroundColor: "white",
                border: "none",
                cursor: "pointer",
              }}
              onClick={() => onLogout()}
            >
              {t("common:loggedOut.signOut")}
            </button>
          </div>
          <LangSelect />
          {/* </div> */}
        </div>
      </section>
    </>
  );
};

const LoggedOutMenu = ({ onLogin }) => {
  const { t } = useTranslation("common");
  return (
    <>
      <section className={`${styles["auth-menu-layout"]}`}>
        <header
          className={`${styles["layout-header"]} d-flex align-items-center`}
        >
          <div className="flex-grow-1">
            <img
              src="/img/landing-page/logo.png"
              alt="logo"
              width={170}
              height={42}
            />
          </div>
          <div>
            <img
              src="/img/icons/menu-burger.png"
              alt="menu"
              width={20}
              height={20}
            />
          </div>
        </header>
        <ul className="p-0 my-3 flex-grow-1" style={{ listStyle: "none" }}>
          <li className="p-1 m-1 pointer fw-bolder">
            {t("common:loggedIn.dashboard")}
          </li>
          <li className="p-1 m-1 pointer fw-bolder">
            {t("common:loggedIn.privateAsso")}
          </li>
          <li className="p-1 m-1 pointer fw-bolder" onClick={() => onLogin()}>
            {t("common:loggedIn.user")}
          </li>
          <li className="p-1 m-1 pointer fw-bolder">
            {t("common:loggedIn.aboutUs")}
          </li>
        </ul>
        <div className={`${styles["bottom-actions"]}`}>
          <LangSelect />
        </div>
      </section>
    </>
  );
};

const AuthMenu = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const onLogin = () => {
    setIsAuthenticated(true);
  };

  const onLogout = () => {
    setIsAuthenticated(false);
  };

  return (
    <>
      {isAuthenticated ? (
        <>
          <LoggedInMenu onLogout={onLogout} />
        </>
      ) : (
        <>
          <LoggedOutMenu onLogin={onLogin} />
        </>
      )}
    </>
  );
};

export default AuthMenu;
