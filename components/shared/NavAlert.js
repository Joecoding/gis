import Link from "next/link";

import styles from "./../../styles/shared/NavAlert.module.scss";

const NavAlert = ({ alertText, setIsAlert }) => {
  console.log(alertText);
  return (
    <>
      <div id={styles["nav-alert"]}>
        <div className="text-white container py-2">
          <div className="d-flex align-items-center">
            <div className="flex-grow-1">
              <Link href="/#">
                <>{alertText}</>
              </Link>
            </div>
            <button
              className="btn-primary p-2 px-3"
              onClick={() => setIsAlert(false)}
            >
              x
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default NavAlert;
