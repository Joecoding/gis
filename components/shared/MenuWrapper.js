import { useState } from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

export default function MenuWrapper({
  anchorEl,
  open,
  handleClose,
  anchor,
  transform,
  children,
  sx,
}) {
  if (anchorEl === undefined || open === undefined || !handleClose || !anchor) {
    return <div>Menu is not configured properly</div>;
  }
  return (
    <div>
      {/* {handler} */}
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={anchor}
        transformOrigin={transform}
        sx={{
          "& .MuiPaper-root": {
            borderRadius: "10px",
            boxShadow: "2px 2px 6px -3px black",
          },
          "& .MuiList-root": {
            paddingTop: "0px",
            paddingBottom: "0px",
          },
          ...sx,
        }}
        // MenuListProps={{
        //   "aria-labelledby": "basic-button",
        // }}
      >
        {children}
      </Menu>
    </div>
  );
}
