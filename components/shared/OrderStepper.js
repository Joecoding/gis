import React, { Fragment, useState, useEffect } from "react";

import styles from "../../styles/shared/OrderStepper.module.scss";

export default function OrderStepper({ totalSteps, currentStep, refused }) {
  const [onStep, setOnStep] = useState(currentStep);

  useEffect(() => {
    if (refused === true) {
      setOnStep(totalSteps);
    }
  }, [currentStep, refused]);

  return (
    <>
      <div
        className={`${styles["stepper-container"]} py-4 mx-auto`}
        style={{ maxWidth: 120 * totalSteps + "px", minWidth: "200px" }}
      >
        <div className={`${styles["stepper-line"]}`}></div>
        <div
          className={`${styles["dots-container"]} d-flex align-items-center justify-content-between`}
        >
          {[...Array(totalSteps)].map((x, i) => (
            <Fragment key={i}>
              {i + 1 === onStep ? (
                <div
                  className={`${styles["stepper-dot"]} ${
                    refused ? styles["refused"] : styles["active"]
                  }`}
                  data-
                ></div>
              ) : (
                <div className={`${styles["stepper-dot"]}`}></div>
              )}
            </Fragment>
          ))}
        </div>
      </div>
    </>
  );
}
