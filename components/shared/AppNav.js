import React from "react";

import SidebarMenuButton from "./buttons/SidebarMenuButton";

import styles from "../../styles/shared/AppNav.module.scss";

const AppNav = ({ header, menu }) => {
  return (
    <>
      <div id={styles["app-nav"]}>
        <div className="container">
          <div className="d-flex align-items-center py-5 px-3">
            {/* <h4 className="flex-grow-1 text-white">Restuarant Name</h4> */}
            <div className="flex-grow-1">{header}</div>
            <SidebarMenuButton>{menu}</SidebarMenuButton>
          </div>
        </div>
      </div>
      {/* <div
        style={{
          backgroundImage: "url(/img/bagel-cover.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "450px",
          backgroundPosition: "center center",
        }}
      ></div> */}
    </>
  );
};

export default AppNav;
