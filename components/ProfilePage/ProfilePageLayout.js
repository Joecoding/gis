import React from "react";
import useTranslation from "next-translate/useTranslation";
import { useRouter } from "next/router";
import Link from "next/link";

import AuthMenu from "../shared/AuthMenu";
import AppNav from "../shared/AppNav";

import Paper from "@mui/material/Paper";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

const NavHeader = ({ t }) => {
  return <h4 className="text-white">{t("profileCommon:appHeader.title")}</h4>;
};

const ProfilePageLayout = ({ children }) => {
  const router = useRouter();
  const { t } = useTranslation("profileCommon");

  return (
    <>
      <AppNav header={<NavHeader t={t} />} menu={<AuthMenu />} />
      <main className="container" style={{ paddingTop: "250px" }}>
        <div className="row">
          <div className="d-none d-md-block col-md-1"></div>
          <div className="col-md-10">
            <div className="row">
              <div
                className="d-none d-sm-block col-sm-3"
                style={{ position: "sticky", top: "0px", height: "100vh" }}
              >
                <section className="my-3">
                  <Paper
                    sx={{
                      borderRadius: "20px",
                      overflow: "hidden",
                      // boxShadow: "2px 2px 2px 6px black",
                    }}
                    elevation={3}
                  >
                    <h4 className="px-3 pt-3 mb-0 text-secondary">
                      {t("profileCommon:sidebar.activity")}
                    </h4>
                    <div className="py-2">
                      <Link href="/profile" passHref>
                        <ListItem
                          sx={
                            router.pathname === "/profile"
                              ? {
                                  fontWeiht: "700",
                                  color: "white",
                                  backgroundColor: "#fe9139",
                                }
                              : { fontWeight: "700" }
                          }
                          disablePadding
                        >
                          <ListItemButton>
                            <ListItemText>
                              <h6 className="m-0">
                                {t("profileCommon:sidebar.modify")}
                              </h6>
                            </ListItemText>
                          </ListItemButton>
                        </ListItem>
                      </Link>
                      <Link href="/profile/recently-viewed" passHref>
                        <ListItem
                          sx={
                            router.pathname === "/profile/recently-viewed"
                              ? {
                                  fontWeiht: "700",
                                  color: "white",
                                  backgroundColor: "#fe9139",
                                }
                              : { fontWeight: "700" }
                          }
                          disablePadding
                        >
                          <ListItemButton>
                            <ListItemText>
                              <h6 className="m-0">
                                {t("profileCommon:sidebar.recently")}
                              </h6>
                            </ListItemText>
                          </ListItemButton>
                        </ListItem>
                      </Link>
                    </div>
                  </Paper>
                </section>
                <section className="my-3">
                  <Paper
                    sx={{
                      borderRadius: "20px",
                      overflow: "hidden",
                      // boxShadow: "2px 2px 2px 6px black",
                    }}
                    elevation={3}
                  >
                    <h4 className="px-3 pt-3 mb-0 text-secondary">
                      {t("profileCommon:sidebar.onlineOrder")}
                    </h4>
                    <div className="py-2">
                      <Link href="/profile/history" passHref>
                        <ListItem
                          sx={
                            router.pathname === "/profile/history"
                              ? {
                                  fontWeiht: "700",
                                  color: "white",
                                  backgroundColor: "#fe9139",
                                }
                              : { fontWeight: "700" }
                          }
                          disablePadding
                        >
                          <ListItemButton>
                            <ListItemText>
                              <h6 className="m-0">
                                {t("profileCommon:sidebar.history")}
                              </h6>
                            </ListItemText>
                          </ListItemButton>
                        </ListItem>
                      </Link>
                      <Link href="/profile/discounts" passHref>
                        <ListItem
                          sx={
                            router.pathname === "/profile/discounts"
                              ? {
                                  fontWeiht: "700",
                                  color: "white",
                                  backgroundColor: "#fe9139",
                                }
                              : { fontWeight: "700" }
                          }
                          disablePadding
                        >
                          <ListItemButton>
                            <ListItemText>
                              <h6 className="m-0">
                                {t("profileCommon:sidebar.discount")}
                              </h6>
                            </ListItemText>
                          </ListItemButton>
                        </ListItem>
                      </Link>
                    </div>
                  </Paper>
                </section>
              </div>
              <div className="col-sm-9">{children}</div>
            </div>
          </div>
          <div className="d-none d-md-block col-md-1"></div>
        </div>
      </main>
    </>
  );
};

export default ProfilePageLayout;
