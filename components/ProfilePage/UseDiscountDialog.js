import React, { useState } from "react";
import useTranslation from "next-translate/useTranslation";

import DialogWrapper from "../shared/DialogWrapper";

const UseDiscountDialog = ({ children }) => {
  console.log(children);

  const { t } = useTranslation("profile");

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
      >
        <div className="py-2">
          <h5 className="mb-3 text-center fw-bolder">
            Are you sure you want to unlock this discount?
          </h5>
          <div className="d-flex align-items-center justify-content-center gap-3">
            <button className="btn-secondary px-2 py-1">OUI</button>
            <button
              onClick={() => handleClose()}
              style={{
                background: "none",
                border: "none",
                fontSize: "1rem",
                fontFamily: "Nunito",
                cursor: "pointer",
              }}
            >
              NON
            </button>
          </div>
        </div>
      </DialogWrapper>
    </>
  );
};

export default UseDiscountDialog;
