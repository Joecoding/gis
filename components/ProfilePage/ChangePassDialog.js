import React from "react";
import useTranslation from "next-translate/useTranslation";

import DialogWrapper from "../shared/DialogWrapper";

import { TextField } from "@mui/material";

const ChangePassDialog = ({ children, open, handleClickOpen, handleClose }) => {
  const { t } = useTranslation("profileIndex");
  return (
    <>
      <div>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        title={t("profileIndex:passwordModal.modalTitle")}
      >
        <div>
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profileIndex:passwordModal.newPassword")}
            </h5>
            <TextField id="outlined-basic" size="small" variant="outlined" />
          </div>
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profileIndex:passwordModal.confirmPassword")}
            </h5>
            <TextField id="outlined-basic" size="small" variant="outlined" />
          </div>
          <div className="d-flex justify-content-end mt-4">
            <button className="btn-primary">
              {/* <AddCircleOutline /> */}
              {t("profileIndex:passwordModal.btn")}
            </button>
          </div>
        </div>
      </DialogWrapper>
    </>
  );
};

export default ChangePassDialog;
