import { Fragment, useState } from "react";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

import Rating from "@mui/material/Rating";

import DialogWrapper from "../shared/DialogWrapper";

import Star from "@mui/icons-material/Star";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";

import styles from "../../styles/ProfilePage/WriteReview.module.scss";
import { Paper } from "@mui/material";

const StepOne = ({ t, setOnStep }) => {
  return (
    <>
      <div className="text-center">
        <img
          src="/img/bagel-cover.jpg"
          alt="profile"
          className="rounded-circle mb-3"
          width={80}
          height={80}
        />
        <h4 classNme="mb-0">
          {t("history:review.step1Title")}
          &nbsp;<a style={{ color: "#fe9139" }}>Bagel Corner</a>&nbsp;?
        </h4>
        <div className="my-2">
          <Rating
            icon={<Star sx={{ color: "#700cbe", fontSize: "40px" }} />}
            sx={{ margin: ".5rem 0rem", fontSize: "40px" }}
          />
        </div>
        <p className="mb-0">
          {t("history:review.orderedOn", {
            date: "24 Jun 2021",
            time: "13:03",
          })}
        </p>
        <div className="d-flex justify-content-center my-3">
          <button
            className="btn-secondary"
            onClick={() => {
              setOnStep(2);
            }}
          >
            {t("history:reivew.send")}
          </button>
        </div>
      </div>
    </>
  );
};
const StepTwo = ({ t, setOnStep }) => {
  return (
    <>
      <div className="text-center">
        <img
          src="/img/bagel-cover.jpg"
          alt="profile"
          className="rounded-circle mb-3"
          width={80}
          height={80}
        />
        <h4 className="mb-0">{t("history:review.leaveComment")}</h4>
        {/* <div className=" my-2"></div> */}
        <Paper
          elevation={3}
          sx={{
            padding: "1rem !important",
            margin: "1rem 0.5rem",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <textarea
            rows="5"
            style={{
              resize: "none",
              border: "none",
              outline: "none",
              fontSize: "1rem",
            }}
          ></textarea>
        </Paper>

        <p className="text-muted mb-0">
          {t("history:review.orderedOn", {
            date: "24 Jun 2021",
            time: "13:03",
          })}
        </p>

        <div className="d-flex justify-content-center my-3">
          <button
            className="btn-secondary"
            onClick={() => {
              setOnStep(3);
            }}
          >
            {t("history:reivew.send")}
          </button>
        </div>
      </div>
    </>
  );
};
const StepThree = ({ t, setOnStep }) => {
  return (
    <>
      <div className="text-center">
        <img
          src="/img/bagel-cover.jpg"
          alt="profile"
          className="rounded-circle mb-3"
          width={80}
          height={80}
        />
        <h4 classNme="mb-2">
          {/* {t("history:review.step1Title")}
          &nbsp;<a style={{ color: "#fe9139" }}>Bagel Corner</a>&nbsp;? */}
          Thank you
        </h4>
        <p className="fw-bolder mb-0">
          Your review for <span className="text-orange">Bagel Corner</span> has
          been published
        </p>
        <div className="my-2">
          <Rating
            icon={<Star sx={{ color: "#700cbe", fontSize: "40px" }} />}
            sx={{ margin: ".5rem 0rem", fontSize: "40px" }}
            readOnly
            value={3}
          />
        </div>
        <div className="d-flex justify-content-center flex-wrap gap-2">
          <div className="btn-secondary">Tag 1</div>
          <div className="btn-secondary">Tag 1</div>
        </div>
        <p className="text-muted my-3 mx-md -5">
          Lorem ipsum Lorem ipsumLorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
          Lorem ipsum Lorem ipsum
        </p>
        <p className="fw-bolder mb-1">You have won 50 Geeds!</p>
        <div className="d-flex justify-content-center mb-3">
          <button className="btn-primary">+50 Geeds</button>
        </div>
      </div>
    </>
  );
};

const WriteReviewDialog = ({ children }) => {
  const { t } = useTranslation("history");

  // For handling Dialog
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [onStep, setOnStep] = useState(1);

  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
      >
        <>
          {onStep === 1 ? (
            <>
              <StepOne t={t} setOnStep={setOnStep} />
            </>
          ) : onStep === 2 ? (
            <>
              <StepTwo t={t} setOnStep={setOnStep} />
            </>
          ) : onStep === 3 ? (
            <>
              <StepThree t={t} setOnStep={setOnStep} />
            </>
          ) : (
            ""
          )}
          <div className="d-flex align-items-center justify-content-center gap-2 my-3">
            <div
              className={`${styles["nav-dot"]} ${
                onStep === 1 ? styles["active"] : ""
              }`}
              onClick={() => setOnStep(1)}
            ></div>
            <div
              className={`${styles["nav-dot"]} ${
                onStep === 2 ? styles["active"] : ""
              }`}
              onClick={() => setOnStep(2)}
            ></div>
            <div
              className={`${styles["nav-dot"]} ${
                onStep === 3 ? styles["active"] : ""
              }`}
              onClick={() => setOnStep(3)}
            ></div>
          </div>
        </>
      </DialogWrapper>
    </>
  );
};

export default WriteReviewDialog;
