import { useState } from "react";
import useTranslation from "next-translate/useTranslation";

import DialogWrapper from "../shared/DialogWrapper";

import { TextField } from "@mui/material";
import AddCircleOutline from "@mui/icons-material/AddCircleOutline";

const AddAddressDialog = ({ children }) => {
  const { t } = useTranslation("profile");

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        title={t("profile:addressModal.modalTitle")}
      >
        <div className="my-3">
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profile:addressModal.addressTitle")}
            </h5>
            <TextField id="outlined-basic" size="small" variant="outlined" />
          </div>
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profile:addressModal.addressTitle")}
            </h5>
            <TextField id="outlined-basic" size="small" variant="outlined" />
          </div>
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profile:addressModal.addressTitle")}
            </h5>
            <TextField id="outlined-basic" size="small" variant="outlined" />
          </div>
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profile:addressModal.addressTitle")}
            </h5>
            <TextField id="outlined-basic" size="small" variant="outlined" />
          </div>
          <div className="d-flex justify-content-end mt-4">
            <button className="btn-secondary">
              <AddCircleOutline />
              <div className="ms-1">{t("profile:addressModal.btn")}</div>
            </button>
          </div>
        </div>
      </DialogWrapper>
    </>
  );
};

export default AddAddressDialog;
