import { useState } from "react";
import useTranslation from "next-translate/useTranslation";

import DialogWrapper from "../shared/DialogWrapper";

import { TextField } from "@mui/material";
import AddCircleOutline from "@mui/icons-material/AddCircleOutline";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";

const EditInfoDialog = ({ children }) => {
  const { t } = useTranslation("profile");

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        title={t("profile:infoModal.modalTitle")}
      >
         <div className="d-flex gap-6 mb-3">
              <div className="d-flex align-items-center gap-3 flex-grow-1">
                <img
                  src="/img/bagel-cover.jpg"
                  alt="profile-img"
                  width={100}
                  height={100}
                  className="rounded-circle"
                />
                <div>
                  <h5 className="m-0 text-primary fw-bolder">
                    {t("profileIndex:infoSec.edit")}
                  </h5>
                  <small
                    className="text-muted"
                    // style={{ lineHeight: "0.8em" }}
                  >
                    {t("profileIndex:infoSec.editInfo")}
                  </small>
                </div>
              </div>
              
              <div>
              <img
                  src="/img/edit.png"
                  alt="edit-img"
                  width={30}
                  height={30}
                  sx={{ color: "red" }}
          
                />
              </div>
              
            </div>
      </DialogWrapper>
    </>
  );
};

export default EditInfoDialog;
