import { Fragment, useState } from "react";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

import DialogWrapper from "../shared/DialogWrapper";
import MenuWrapper from "../shared/MenuWrapper";

import { TextField } from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";

const BankCardComponent = ({ t }) => {
  return (
    <>
      <div>
        <div className="my-2 d-flex flex-column">
          <label className="fw-bolder mb-2">
            {t("profileIndex:bankCard.cardNum")}
          </label>
          <TextField size="small" />
        </div>
        <div className="my-2 d-flex flex-column">
          <label className="fw-bolder mb-2">
            {t("profileIndex:bankCard.expiration")}
          </label>
          <div className="d-flex gap-3">
            <TextField sx={{ width: "100%" }} size="small" />
            <TextField sx={{ width: "100%" }} size="small" />
          </div>
        </div>
        <div className="my-2 d-flex flex-column">
          <label className="fw-bolder mb-2">
            {t("profileIndex:bankCard.cardNum")}
          </label>
          <TextField
            size="small"
            helperText={t("profileIndex:bankCard.cvvHelper")}
          />
        </div>
        <div className="d-flex justify-content-end mt-4">
          <button className="btn-primary">
            <div>{t("profileIndex:bankCard.save")}</div>
          </button>
        </div>
      </div>
    </>
  );
};
const TicketCardComponent = ({ t }) => {
  const [tickets] = useState([
    {
      name: "restoFlash",
      img: "/img/tickets/resto-flash.png",
    },
    {
      name: "apetiz",
      img: "/img/tickets/apetiz.png",
    },
    {
      name: "swile",
      img: "/img/tickets/swile.png",
    },
  ]);
  const [selectedTicket, setSelectedTicket] = useState(null);

  return (
    <>
      {!selectedTicket ? (
        <>
          <h5>{t("profileIndex:ticketCard.acceptedTickets")}</h5>
          <List sx={{ maxHeight: "280px", overflowX: "auto" }}>
            {tickets.length ? (
              <>
                {tickets.map((ticket) => {
                  return (
                    <>
                      <ListItem
                        disablePadding
                        onClick={() => setSelectedTicket(ticket)}
                      >
                        <ListItemButton>
                          <ListItemText>
                            <div className="d-flex align-items-center">
                              <div className="d-flex align-items flex-grow-1">
                                <img
                                  src={ticket.img}
                                  alt={ticket.name}
                                  // width={100}
                                  height={30}
                                />
                              </div>

                              <KeyboardArrowRightIcon />
                            </div>
                          </ListItemText>
                        </ListItemButton>
                      </ListItem>
                    </>
                  );
                })}
              </>
            ) : (
              ""
            )}
          </List>
          <div className="d-flex justify-content-end mt-4">
            <button className="btn-primary">
              {/* <AddCircleOutline /> */}
              <div>{t("profileIndex:cardModal.following")}</div>
            </button>
          </div>
        </>
      ) : (
        <>
          <div className="d-flex justify-content-center">
            <img
              src={selectedTicket.img}
              alt={selectedTicket.name}
              // width={100}
              height={50}
            />
          </div>
          <div className="px-5 py-3">
            <p className="m-0">
              Lorem Ipsum Lorem IpsumLorem IpsumLorem IpsumLorem Ipsum Lorem
              Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
            </p>
            <p className="m-0">
              Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum Lorem
              Ipsum Lorem Ipsum
            </p>
          </div>
          <div className="d-flex align-items-center flex-column gap-3">
            <div>
              <button className="btn-primary">
                {t("profileIndex:ticketCard.logIn")}
              </button>
            </div>
            <div>
              <Link href="/#">{t("profileIndex:ticketCard.createAcc")}</Link>
            </div>
          </div>
          <div className="d-flex justify-content-end mt-4">
            <button className="btn-primary">
              {/* <AddCircleOutline /> */}
              <div>{t("profileIndex:cardModal.save")}</div>
            </button>
          </div>
        </>
      )}
    </>
  );
};
const PaypalComponent = ({ t }) => {
  return (
    <>
      <div>
        <p>
          Lorem Ipsum Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum
          Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum
        </p>
        <div className="d-flex justify-content-end mt-4">
          <button className="btn-primary">
            <div>{t("profileIndex:paypalCard.btn")}</div>
          </button>
        </div>
      </div>
    </>
  );
};

const AddCardDialog = ({ children }) => {
  const { t } = useTranslation("profile");

  // For handling Dialog
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // for type menu
  const [typeAnchorEl, setTypeAnchorEl] = useState(null);
  const [isTypeFocused, setIsFocused] = useState(false);
  const handleTypeMenuOpen = (event) => {
    setTypeAnchorEl(event.currentTarget);
    setIsFocused(true);
  };
  const handleTypeMenuClose = () => {
    setTypeAnchorEl(null);
    setIsFocused(false);
  };

  // for Handling Dialog form state
  const [cardTypes] = useState([
    t("profileIndex:cardModal.type2"),
    t("profileIndex:cardModal.type1"),
    t("profileIndex:cardModal.type3"),
  ]);
  const [currentType, setCurrentType] = useState(cardTypes[0]);

  return (
    <>
      <div onClick={handleClickOpen}>{children}</div>
      <DialogWrapper
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        title={t("profileIndex:cardModal.modalTitle")}
      >
        <div className="my-3">
          <div className="d-flex flex-column my-3">
            <h5 className="fw-bolder m-0">
              {t("profileIndex:cardModal.typeCard")}
            </h5>
            <div
              className="d-flex align-items-center gap-1 my-1 fw-bolder"
              style={{
                cursor: "pointer",
                background: "#dddddd",
                borderRadius: "14px",
                padding: "8px 14px",
                // color: "white",
                width: "300px",
                fontSize: "1rem",
              }}
              onClick={handleTypeMenuOpen}
            >
              <div className="flex-grow-1">{currentType}</div>
              <KeyboardArrowDownIcon />
            </div>
            <MenuWrapper
              anchorEl={typeAnchorEl}
              open={isTypeFocused}
              handleClose={handleTypeMenuClose}
              anchor={{ vertical: "top", horizontal: "left" }}
              transform={{ vertical: "top", horizontal: "left" }}
              sx={{
                "& .MuiPaper-root": {
                  borderRadius: "14px",
                  boxShadow: "none",
                },
              }}
            >
              {cardTypes.map((type, i) => {
                return (
                  <Fragment key={i}>
                    <li
                      className="fw-bolder"
                      onClick={() => {
                        setCurrentType(type);
                        handleTypeMenuClose();
                      }}
                      style={{
                        width: "100%",
                        backgroundColor: "#dddddd",
                        border: "none",
                        // padding: "8px 14px",
                        // color: "white",
                        fontSize: "1rem",
                        width: "300px",
                        cursor: "pointer",
                      }}
                    >
                      {i !== 0 ? (
                        <>
                          <div style={{ padding: "0px 14px" }}>
                            <hr
                              style={{
                                margin: "0px",
                                border: "none",
                                height: "1px",
                                backgroundColor: "black",
                              }}
                            />
                          </div>
                        </>
                      ) : (
                        ""
                      )}
                      <div style={{ padding: "8px 14px" }}>{type}</div>
                    </li>
                  </Fragment>
                );
              })}
            </MenuWrapper>
          </div>

          {currentType === "Bank Card" ? (
            <>
              <BankCardComponent t={t} />
            </>
          ) : currentType === "Restaurant Ticket Card" ? (
            <>
              <TicketCardComponent t={t} />
            </>
          ) : currentType === "Paypal" ? (
            <>
              <PaypalComponent t={t} />
            </>
          ) : (
            ""
          )}
        </div>
      </DialogWrapper>
    </>
  );
};

export default AddCardDialog;
