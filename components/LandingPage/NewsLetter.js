import { useState, useEffect } from "react";
import styles from "./../../styles/Home.module.scss";
import Carousel from "react-elastic-carousel";


const NewsLetter = ({t}) => {

    const [carouselFour] = useState([
        { width: 0, itemsToShow: 1, itemsToScroll: 1 },
        { width: 567, itemsToShow: 2, itemsToScroll: 1 },
        { width: 768, itemsToShow: 4, itemsToScroll: 1 },
    ]);

    const [carouselThree] = useState([
        { width: 0, itemsToShow: 1, itemsToScroll: 1 },
        { width: 567, itemsToShow: 2, itemsToScroll: 1 },
        { width: 768, itemsToShow: 3, itemsToScroll: 1 },
    ]);


     useEffect(() => {
        //api goes here to fetch data 

      })




      return (
          <>
                    <section id={styles["food-aid-section"]}>
                                    <div className="container py-8">
                                        <div className="row">
                                            <div className="col-md-7">
                                                <div className={styles["food-aid"]}>
                                                    <h3
                                                        className={`${styles.header} text-white`}
                                                    >
                                                        {t("home:foodAid.title")}
                                                    </h3>
                                                    <div
                                                        className={`${styles["aid-card"]} d-flex flex-column`}
                                                    >
                                                        <img
                                                            src="/img/landing-page/aid.png"
                                                            alt="Food Aid Program"
                                                            className="w-100 m-0"
                                                        />
                                                        <h4
                                                            className={`${styles["aid-card-text"]} text-accent-primary fw-bolder px-5 py-3 m-0`}
                                                        >
                                                            {t("home:foodAid.imgTitle")}
                                                        </h4>
                                                    </div>
                                                    <div className="text-white m-4">
                                                        <p>{t("home:foodAid.text1")}</p>
                                                        <p>{t("home:foodAid.text2")}</p>
                                                    </div>
                                                    <div className="mx-4 d-flex align-items-center">
                                                        <div>
                                                            <button className="btn-primary btn-shadow me-3">
                                                                {t("home:foodAid.btn1")}
                                                            </button>
                                                        </div>
                                                        <div>
                                                            <button className="btn-primary btn-shadow">
                                                                {t("home:foodAid.btn2")}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-5 d-none d-md-block">
                                                <div className={styles["mockup-container"]}>
                                                    <img
                                                        src="/img/landing-page/mock.png"
                                                        alt="Food Aid Program"
                                                        className="w-100 m-0"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    </section>
                    <section id={styles["get-app-section"]}>
                        <div className="container py-8">
                            <div className="row">
                                <div className="col-md-7">
                                    <div className="mockup-container">
                                        <img
                                            src="/img/landing-page/mockup2.png"
                                            alt="Get the mobile app"
                                            className="w-100 m-0"
                                        />
                                    </div>
                                </div>
                                <div className="col-md-5 my-3 d-flex flex-column justify-content-center align-items-center align-items-md-start">
                                    <h3 className="text-accent-primary">
                                        {t("home:getApp.title")}
                                    </h3>
                                    <ul
                                        className={`${styles["feature-list"]} my-3 px-0`}
                                    >
                                        <li className=" d-flex align-items-center gap-3">
                                            <div
                                                className={`${styles["feature-count"]}`}
                                            >
                                                1
                                            </div>
                                            <h4 className="m-0">
                                                {t("home:getApp.point1")}
                                            </h4>
                                        </li>
                                        <li className=" d-flex align-items-center gap-3">
                                            <div
                                                className={`${styles["feature-count"]}`}
                                            >
                                                2
                                            </div>
                                            <h4 className="m-0">
                                                {t("home:getApp.point2")}
                                            </h4>
                                        </li>
                                        <li className=" d-flex align-items-center gap-3">
                                            <div
                                                className={`${styles["feature-count"]}`}
                                            >
                                                3
                                            </div>
                                            <h4 className="m-0">
                                                {t("home:getApp.point3")}
                                            </h4>
                                        </li>
                                    </ul>
                                    <div className="d-flex align-items-center gap-3 mt-4">
                                        <button className="app-btn">
                                            <img
                                                src="/img/landing-page/playstore.png"
                                                alt="Download Android App"
                                                width={130}
                                            />
                                        </button>
                                        <button className="app-btn">
                                            <img
                                                src="/img/landing-page/appstore.png"
                                                alt="Download IOS App"
                                                width={130}
                                            />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        id={styles["display-cards"]}
                        className="container my-7"
                    >
                        <div className="row">
                            <div className="col-md-4 py-4 px-lg-5 px-md-4 px-3">
                                <div className={styles["display-card"]}>
                                    <img
                                        className="w-100"
                                        src="/img/landing-page/dcard1.png"
                                        alt="Highlight"
                                    />
                                    <div>
                                        <div
                                            className={`${styles["svg"]} d-flex flex-column`}
                                        >
                                            <img
                                                src="/img/landing-page/display-purple.png"
                                                className="w-100"
                                            />
                                            <div className={styles.title}>
                                                Card Title
                                            </div>
                                            <div className={styles.text}>
                                                Lorem Ipsunm Lorem Ipsunm Lorem
                                                Ipsunm Lorem Ipsunm Lorem Ipsunm
                                                Lorem Ipsunm Lorem Ipsunm Lorem
                                                Ipsunm Lorem Ipsunm Lorem Ipsunm
                                            </div>
                                            <div className={styles.btn}>
                                                <button className="btn-secondary">
                                                    Btn Text
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 py-4 px-lg-5 px-md-4 px-3">
                                <div className={styles["display-card"]}>
                                    <img
                                        className="w-100"
                                        src="/img/landing-page/dcard1.png"
                                        alt="Highlight"
                                    />
                                    <div>
                                        <div
                                            className={`${styles["svg"]} d-flex flex-column`}
                                        >
                                            <img
                                                src="/img/landing-page/display-orange.png"
                                                className="w-100"
                                            />
                                            <div className={styles.title}>
                                                Card Title
                                            </div>
                                            <div className={styles.text}>
                                                Lorem Ipsunm Lorem Ipsunm Lorem
                                                Ipsunm Lorem Ipsunm Lorem Ipsunm
                                                Lorem Ipsunm Lorem Ipsunm Lorem
                                                Ipsunm Lorem Ipsunm Lorem Ipsunm
                                            </div>
                                            <div className={styles.btn}>
                                                <button className="btn-primary">
                                                    Btn Text
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 py-4 px-lg-5 px-md-4 px-3">
                                <div className={styles["display-card"]}>
                                    <img
                                        className="w-100"
                                        src="/img/landing-page/dcard1.png"
                                        alt="Highlight"
                                    />
                                    <div>
                                        <div
                                            className={`${styles["svg"]} d-flex flex-column`}
                                        >
                                            <img
                                                src="/img/landing-page/display-purple.png"
                                                className="w-100"
                                            />
                                            <div className={styles.title}>
                                                Card Title
                                            </div>
                                            <div className={styles.text}>
                                                Lorem Ipsunm Lorem Ipsunm Lorem
                                                Ipsunm Lorem Ipsunm Lorem Ipsunm
                                                Lorem Ipsunm Lorem Ipsunm Lorem
                                                Ipsunm Lorem Ipsunm Lorem Ipsunm
                                            </div>
                                            <div className={styles.btn}>
                                                <button className="btn-secondary">
                                                    Btn Text
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id={styles.testimonial} className="container my-5">
                        <h3 className="text-muted fw-bolder flex-grow-1">
                            {t("home:testimonial.title")}
                        </h3>
                        <Carousel
                            pagination={false}
                            breakPoints={carouselThree}
                        >
                            <div
                                className={`${styles["testimonial-item"]} m-3`}
                            >
                                <img
                                    src="/img/landing-page/tache1.png"
                                    className="w-100"
                                    // width={100}
                                    // height={100}
                                />
                                <div className={styles.text}>
                                    “Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore” dolore magna aliqua.
                                    Quis ipsum suspendisse ultrices gravida.
                                    Risus commodo viverra maecenas accumsan
                                    lacus vel facilisis.
                                </div>
                                <div className={styles.name}>Lola</div>
                                <div className={styles.img}>
                                    <img src="/img/landing-page/consumer.png" />
                                </div>
                            </div>
                            <div
                                className={`${styles["testimonial-item"]} m-3`}
                            >
                                <img
                                    src="/img/landing-page/tache2.png"
                                    className="w-100"
                                    // width={100}
                                    // height={100}
                                />
                                <div className={styles.text}>
                                    “Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore” dolore magna aliqua.
                                    Quis ipsum suspendisse ultrices gravida.
                                    Risus commodo viverra maecenas accumsan
                                    lacus vel facilisis.
                                </div>
                                <div className={styles.name}>Lola</div>
                                <div className={styles.img}>
                                    <img src="/img/landing-page/consumer.png" />
                                </div>
                            </div>
                            <div
                                className={`${styles["testimonial-item"]} m-3`}
                            >
                                <img
                                    src="/img/landing-page/tache3.png"
                                    className="w-100"
                                    // width={100}
                                    // height={100}
                                />
                                <div className={styles.text}>
                                    “Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore” dolore magna aliqua.
                                    Quis ipsum suspendisse ultrices gravida.
                                    Risus commodo viverra maecenas accumsan
                                    lacus vel facilisis.
                                </div>
                                <div className={styles.name}>Lola</div>
                                <div className={styles.img}>
                                    <img src="/img/landing-page/consumer.png" />
                                </div>
                            </div>
                        </Carousel>
                    </section>
                    <section id={styles.partners} className="container my-5">
                        <h3 className="text-muted fw-bolder flex-grow-1">
                            {t("home:partner.title")}
                        </h3>
                        <Carousel pagination={false} breakPoints={carouselFour}>
                            {/* <div className="row mx-5 mt-3 mb-6"> */}
                            <div
                                className="m-3"
                                style={{
                                    borderRadius: "25px",
                                    boxShadow: "1px 9px 20px -10px #000000",
                                }}
                            >
                                <img
                                    src="/img/landing-page/partner1.png"
                                    alt="Partners"
                                    className="w-100"
                                />
                            </div>
                            {/* <div className="col-lg-3 col-md-6"></div>
                            <div className="col-lg-3 col-md-6"></div>
                            <div className="col-lg-3 col-md-6"></div> */}
                            {/* </div> */}
                        </Carousel>
                    </section>
                    <section id={styles["newsletter"]} className="my-7">
                        <div className={styles["newsletter-container"]}>
                            <form>
                                <h5 className="text-white text-center">
                                    {t("home:newsletter.title")}
                                </h5>
                                <div className="field-inline-text rounded-pill">
                                    <input
                                        type="text"
                                        placeholder={t(
                                            "home:newsletter.placeholder"
                                        )}
                                    ></input>
                                    <button className="btn-secondary rounded-pill px-3">
                                        {t("home:newsletter.btn")}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section id={styles["latest-updates"]}>
                        <h3 className="text-center text-muted fw-bolder">
                            {t("home:latestUpdates.title")}
                        </h3>
                        <div className="container">
                            <div className="row my-6">
                                <div className="col-md-3 col-sm-6 p-4 p-md-0 px-md-2 px-lg-4">
                                    <img 
                                        src="/img/landing-page/update1.png"
                                        alt="Updates"
                                        className="w-100"
                                    />
                                </div>
                                <div className="col-md-3 col-sm-6 p-4 p-md-0 px-md-2 px-lg-4">
                                    <img
                                        src="/img/landing-page/update2.png"
                                        alt="Updates"
                                        className="w-100"
                                    />
                                </div>
                                <div className="col-md-3 col-sm-6 p-4 p-md-0 px-md-2 px-lg-4">
                                    <img
                                        src="/img/landing-page/update3.png"
                                        alt="Updates"
                                        className="w-100"
                                    />
                                </div>
                                <div className="col-md-3 col-sm-6 p-4 p-md-0 px-md-2 px-lg-4">
                                    <img
                                        src="/img/landing-page/update4.png"
                                        alt="Updates"
                                        className="w-100"
                                    />
                                </div>
                            </div>
                        </div>
                    </section>
                    
          </>
      );

}

export default NewsLetter;