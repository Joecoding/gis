import { Fragment, useState, useEffect } from "react";
import Link from "next/link";

import MenuWrapper from "../shared/MenuWrapper";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

import styles from "./../../styles/LandingPage/Hero.module.scss";

const Hero = ({ t }) => {
    const [orderTypes, setOrderTypes] = useState([
        { text: t("home:search.op1"), img: "/img/icons/restaurant-icon.svg" },
        { text: t("home:search.op2"), img: "/img/icons/shopping-bag-icon.svg" },
        { text: t("home:search.op3"), img: "/img/icons/bicycle-icon.svg" },
    ]);
    const [currentOrderType, setCurrentOrderType] = useState({
        text: t("home:search.op1"),
        img: "/img/icons/restaurant-icon.svg",
    });

    const [currentHeader, setCurrentHeader] = useState("");
    const [headers] = useState([
        {
            img: "/img/landing-page/hero-1.png",
        },
        {
            img: "/img/landing-page/hero-2.png",
        },
        {
            img: "/img/landing-page/hero-3.png",
        },
    ]);

    useEffect(() => {
        setCurrentHeader(
            headers[Math.floor(Math.random() * headers.length)].img
        );
    }, []);

    // const [anchorEl, setAnchorEl] = useState(null);
    const [isOpen, setIsOpen] = useState(false);
    const handleClick = (event) => {
        // setAnchorEl(event.currentTarget);
        setIsOpen(true);
    };
    const handleClose = () => {
        // setAnchorEl(null);
        setIsOpen(false);
    };

    return (
        <>
            <section
                className="d-flex align-items-center w-100"
                style={{
                    height: "110vh",
                    backgroundImage: `url(${currentHeader})`,
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                    backgroundPosition: "center right",
                }}
            >
                <div className="container">
                    <div
                        className={`${styles["hero"]} mx-md-5`}
                        style={{ marginTop: "-4rem" }}
                    >
                        <h1 className="dual-color-head mb-4 text-shadow">
                            <span>{t("home:bannerText1")} </span>
                            {t("home:bannerText2")}
                        </h1>
                        <form onSubmit={(e) => e.preventDefault()}>
                            <div
                                className={`d-md-flex align-items-center gap-3`}
                            >
                                {/* <select className={`${styles["dropdown"]}`} onChange={() => console.log("working")}>
                  <option className={`${styles["option"]}`}>On the Spot</option>
                  <option className={`${styles["option"]}`}>Takeaway</option>
                  <option className={`${styles["option"]}`}>Delivery</option>
                </select> */}

                                <div
                                    className={`${styles["dropdown-container"]} my-3`}
                                >
                                    <div
                                        className={
                                            isOpen
                                                ? `${styles["dropdown"]} ${styles["active"]} d-flex align-items-center gap-2 fw-bolder`
                                                : `${styles["dropdown"]} d-flex align-items-center gap-2 fw-bolder`
                                        }
                                        onClick={() => setIsOpen(!isOpen)}
                                        // onKeyDown={(e) => {console.log(e)}}
                                    >
                                        <img
                                            src={currentOrderType.img}
                                            alt={currentOrderType.text}
                                            height={25}
                                            width={25}
                                        />
                                        <div className="flex-grow-1">
                                            {currentOrderType.text}
                                        </div>
                                        <KeyboardArrowDownIcon />
                                    </div>
                                    {isOpen ? (
                                        <ul
                                            className={`${styles["dropdown-list"]}`}
                                        >
                                            {orderTypes.map((type, i) => {
                                                return (
                                                    <li
                                                        key={i}
                                                        className={
                                                            currentOrderType.text ===
                                                            type.text
                                                                ? `${styles["hidden"]} d-flex align-items-center gap-2 fw-bolder`
                                                                : `d-flex align-items-center gap-2 fw-bolder`
                                                        }
                                                        onClick={() => {
                                                            setCurrentOrderType(
                                                                type
                                                            );
                                                            handleClose();
                                                        }}
                                                    >
                                                        <img
                                                            src={type.img}
                                                            alt={type.text}
                                                            height={25}
                                                            width={25}
                                                        />
                                                        <div>{type.text}</div>
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    ) : (
                                        ""
                                    )}
                                </div>
                                <div
                                    className={`${styles["icon-field"]} d-flex align-items-center gap-2 my-3`}
                                >
                                    <img
                                        src="/img/icons/location-icon.svg"
                                        height={25}
                                        width={25}
                                    />
                                    <input
                                        type="text"
                                        placeholder={t("home:search.location")}
                                    />
                                </div>
                                <div
                                    className={`${styles["icon-field"]} ${styles["field-btn"]} d-flex align-items-center gap-2 my-3`}
                                >
                                    <img
                                        src="/img/icons/search-icon.svg"
                                        height={25}
                                        width={25}
                                    />
                                    <input
                                        type="text"
                                        placeholder={t("home:search.food")}
                                    />
                                    <Link href="/search" passHref>
                                        <button
                                            className="btn-primary rounded-pill py-1 px-2"
                                            type="submit"
                                        >
                                            <ArrowForwardIcon />
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </>
    );
};

export default Hero;
