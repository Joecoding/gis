import styles from "./../../styles/Home.module.scss";
import Carousel from "react-elastic-carousel";
import { useState, useEffect } from "react";
import Link from "next/link";



const CarouselCard = ({ t }) => {

    const [carouselFour] = useState([
        { width: 0, itemsToShow: 1, itemsToScroll: 1 },
        { width: 567, itemsToShow: 2, itemsToScroll: 1 },
        { width: 768, itemsToShow: 4, itemsToScroll: 1 },
    ]);
  
    useEffect(() => {
        //api goes here to fetch data 

    })
   

   return (

       <>
           
           <section id={styles["card-vertical"]} className="container">
                        <div className="d-flex align-items-center">
                            <h3 className="text-muted fw-bolder flex-grow-1">
                                {t("home:allRestaurants")}
                            </h3>
                            <div className="d-flex align-items-center">
                                <button className="btn-secondary">
                                    {t("home:viewAll")}
                                </button>
                            </div>
                        </div>

                        <div className="row pb-6 pt-2">
                           

                            {/* All Restaurants Carousel Section */}

                            {[ ...Array(2)].map((i) => {

                                            return (
                                                    
                                                <Carousel
                                                    pagination={false}
                                                    breakPoints={carouselFour}
                                                    key={i}
                                                >
                                                    <div className="m-3">
                                                        <img
                                                            className="w-100"
                                                            src="/img/landing-page/arr1.png"
                                                        />
                                                        <div className="m-2">
                                                            <Link href="/restaurants/1/menu">
                                                                <h5
                                                                    className="fw-bolder m-0 pointer"
                                                                    title="Go to Crèperie Hermine"
                                                                >
                                                                    Crèperie Hermine
                                                                </h5>
                                                            </Link>
                                                            <p className="text-muted">
                                                                Français I 4,7 I Livré dans 25min
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="m-3">
                                                        <img
                                                            className="w-100"
                                                            src="/img/landing-page/arr2.png"
                                                        />
                                                    </div>
                                                    <div className="m-3">
                                                        <img
                                                            className="w-100"
                                                            src="/img/landing-page/arr3.png"
                                                        />
                                                    </div>
                                                    <div className="m-3">
                                                        <img
                                                            className="w-100"
                                                            src="/img/landing-page/arr4.png"
                                                        />
                                                    </div>
                                                </Carousel>
                                            );

                                        })}
                           
                        </div>
                    </section>
             
                       {/* Home Round Me Carousel section */}
                    {[ ...Array(4)].map((i)  => {

                            return (

                            <section
                                id={styles["card-horizontal"]}
                                className="container"
                                key={i}
                            >
                                <div className="d-flex align-items-center">
                                    <h3 className="text-muted fw-bolder flex-grow-1">
                                        {t("home:aroundMe")}
                                    </h3>
                                    <div className="d-flex align-items-center">
                                        <button className="btn-secondary">
                                            {t("home:viewAll")}
                                        </button>
                                    </div>
                                </div>
                                <div className="pb-5 pt-2">
                                    <Carousel
                                        pagination={false}
                                        breakPoints={carouselFour}
                                        key={i}
                                    >
                                        <div className="m-3">
                                            <img
                                                src="/img/landing-page/off1.png"
                                                alt="offer"
                                                className="w-100"
                                            />
                                            <div className="d-flex gap-2">
                                                <img
                                                    src="/img/landing-page/arr2.png"
                                                    alt="profile"
                                                    width={50}
                                                    height={50}
                                                    style={{ borderRadius: "100%" }}
                                                />
                                                <div>
                                                    <h5 className="fw-bolder m-0">
                                                        Crèperie Hermine
                                                    </h5>
                                                    <p className="text-muted">
                                                        Français I 4,7 I Livré dans
                                                        25min
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="m-3">
                                            <img
                                                src="/img/landing-page/off2.png"
                                                alt="offer"
                                                className="w-100"
                                            />
                                            <div className="d-flex gap-2">
                                                <img
                                                    src="/img/landing-page/arr2.png"
                                                    alt="profile"
                                                    width={50}
                                                    height={50}
                                                    style={{ borderRadius: "100%" }}
                                                />
                                                <div>
                                                    <h5 className="fw-bolder m-0">
                                                        Crèperie Hermine
                                                    </h5>
                                                    <p className="text-muted">
                                                        Français I 4,7 I Livré dans
                                                        25min
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="m-3">
                                            <img
                                                src="/img/landing-page/off3.png"
                                                alt="offer"
                                                className="w-100"
                                            />
                                            <div className="d-flex gap-2">
                                                <img
                                                    src="/img/landing-page/arr2.png"
                                                    alt="profile"
                                                    width={50}
                                                    height={50}
                                                    style={{ borderRadius: "100%" }}
                                                />
                                                <div>
                                                    <h5 className="fw-bolder m-0">
                                                        Crèperie Hermine
                                                    </h5>
                                                    <p className="text-muted">
                                                        Français I 4,7 I Livré dans
                                                        25min
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="m-3">
                                            <img
                                                src="/img/landing-page/off3.png"
                                                alt="offer"
                                                className="w-100"
                                            />
                                            <div className="d-flex gap-2">
                                                <img
                                                    src="/img/landing-page/arr2.png"
                                                    alt="profile"
                                                    width={50}
                                                    height={50}
                                                    style={{ borderRadius: "100%" }}
                                                />
                                                <div>
                                                    <h5 className="fw-bolder m-0">
                                                        Crèperie Hermine
                                                    </h5>
                                                    <p className="text-muted">
                                                        Français I 4,7 I Livré dans
                                                        25min
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="m-3">
                                            <img
                                                src="/img/landing-page/off3.png"
                                                alt="offer"
                                                className="w-100"
                                            />
                                            <div className="d-flex gap-2">
                                                <img
                                                    src="/img/landing-page/arr2.png"
                                                    alt="profile"
                                                    width={50}
                                                    height={50}
                                                    style={{ borderRadius: "100%" }}
                                                />
                                                <div>
                                                    <h5 className="fw-bolder m-0">
                                                        Crèperie Hermine
                                                    </h5>
                                                    <p className="text-muted">
                                                        Français I 4,7 I Livré dans
                                                        25min
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </Carousel>
                                </div>
                            </section>

                            );
                    })}
                    
                  
         
       </>

      );


};

export default CarouselCard;